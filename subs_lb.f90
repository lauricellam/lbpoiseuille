#define REGULARIZED

#define noCORRTENS
#define CGCORR
#define noNEWEQ
#define noHUANG
    module subs_lb
    
    use vars_lb
    
    implicit none
    
    contains
    
    !**********************************************************!
    subroutine moments(mystep)
      implicit none
      integer, intent(in) :: mystep
      integer :: mm,i,j,ic,flip
        
      flip=mod(step-1,2)+1
        
      if(ncomp==2)then !use different chunks, static do like blocks, private j intend j will be splitted
        !$omp do schedule(static,mychunk) private(j) 
        do j=1,ny
            do i=1,nx
               rho(1,i,j)=0.0d0
               rho(2,i,j)=0.0d0
               uxy(1,i,j)=0.0d0
               uxy(2,i,j)=0.0d0
               if(isfluid_LB(i,j).eq.1)then      
                 do mm=0,8
                   rho(1,i,j)=rho(1,i,j) + fpop(mm,1,i,j,flip) !4.6 of book
                   rho(2,i,j)=rho(2,i,j) + fpop(mm,2,i,j,flip)
                   uxy(1,i,j)=uxy(1,i,j) + fpop(mm,1,i,j,flip)*dex(mm) + fpop(mm,2,i,j,flip)*dex(mm) 
                   uxy(2,i,j)=uxy(2,i,j) + fpop(mm,1,i,j,flip)*dey(mm) + fpop(mm,2,i,j,flip)*dey(mm) 
                 enddo
                 uxy(1,i,j)=uxy(1,i,j)/(rho(1,i,j)+rho(2,i,j)) !4.8 of book
                 uxy(2,i,j)=uxy(2,i,j)/(rho(1,i,j)+rho(2,i,j))
                 if(isnan(rho(1,i,j)) .or. isnan(rho(2,i,j)))then
                   write(6,*)'NAN ',mystep,i,j,rho(1,i,j),rho(2,i,j)
                   stop
                 endif
               endif
            enddo
        enddo
        !$omp end do
      else
        ic=1
        !$omp do schedule(static,mychunk) private(j) 
        do j=1,ny
            do i=1,nx
               rho(ic,i,j)=0.0d0
               uxy(1,i,j)=0.0d0
               uxy(2,i,j)=0.0d0
               if(isfluid_LB(i,j).eq.1)then
                   do mm=0,8
                       rho(ic,i,j)=rho(ic,i,j) + fpop(mm,ic,i,j,flip)
                       uxy(1,i,j)=uxy(1,i,j) + fpop(mm,ic,i,j,flip)*dex(mm)
                       uxy(2,i,j)=uxy(2,i,j) + fpop(mm,ic,i,j,flip)*dey(mm)
                   enddo
                 uxy(1,i,j)=uxy(1,i,j)/rho(ic,i,j)
                 uxy(2,i,j)=uxy(2,i,j)/rho(ic,i,j)
               endif
            enddo
        enddo
        !$omp end do
      endif
    endsubroutine
    
    subroutine correct_momentEDM
    implicit none
    integer :: mm,i,j,flip
     
     !$omp do schedule(static,mychunk) private(j) 
     do j=1,ny
       do i=1,nx
         if(isfluid_LB(i,j).eq.1)then
           uxy(1,i,j)=uxy(1,i,j)+0.5d0*fx_LB/sum(rho(1:ncomp,i,j))
           uxy(2,i,j)=uxy(2,i,j)+0.5d0*fy_LB/sum(rho(1:ncomp,i,j))
         endif
       enddo
      enddo
      !$omp end do
      
      return
      
      end subroutine correct_momentEDM 
    
          
        subroutine moments_2c
        implicit none
        integer :: mm,i,j,flip
        
        flip=mod(step-1,2)+1
        !$omp do schedule(static,mychunk) private(j) 
        do j=1,ny
            do i=1,nx
               if(isfluid_LB(i,j).eq.1)then
                   rho(1,i,j)=0.0d0
                   rho(2,i,j)=0.0d0
                   uxy(1,i,j)=0.0d0
                   uxy(2,i,j)=0.0d0
                   do mm=0,8
                     rho(1,i,j)=rho(1,i,j) + fpop(mm,1,i,j,flip) 
                     rho(2,i,j)=rho(2,i,j) + fpop(mm,2,i,j,flip)
                     uxy(1,i,j)=uxy(1,i,j) + fpop(mm,1,i,j,flip)*dex(mm) + fpop(mm,2,i,j,flip)*dex(mm) 
                     uxy(2,i,j)=uxy(2,i,j) + fpop(mm,1,i,j,flip)*dey(mm) + fpop(mm,2,i,j,flip)*dey(mm) 
                   enddo
                   uxy(1,i,j)=uxy(1,i,j)/(rho(1,i,j)+rho(2,i,j))
                   uxy(2,i,j)=uxy(2,i,j)/(rho(1,i,j)+rho(2,i,j))
               endif
            enddo
        enddo
        !$omp end do
    endsubroutine
    
    !**********************************************************!
    subroutine regularized
	
	integer:: i,j,k,ic,l,flip
	real*8 :: pxx,pyy,pxy
	real*8 :: fneq,oneovcssq_dum,uu,udotc
	real*8, dimension(0:8) :: feq
	
	flip=mod(step-1,2)+1
	if(ncomp.ne.1)then
          write(6,*)'ncomp==2 not written for regularized'
          stop
    endif
	ic=1
    !$omp do schedule(static,mychunk) private(i,l,feq,pxx,pyy,pxy,fneq)
    do j=1,ny
      do i=1,nx
        if(isfluid_LB(i,j).eq.1)then
          pxx=0.0d0
		  pyy=0.0d0
		  pxy=0.0d0
                        !
		  uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
		  do l=0,8
		    udotc=(uxy(1,i,j)*dex(l) + uxy(2,i,j)*dey(l))/cssq	
		    ! page eq 3.4 pag 61 kruger	
		    feq(l)=p(l)*rho(ic,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
		  enddo
						
						!
          do l=0,8
            fneq= fpop(l,ic,i,j,flip) - feq(l)
            pxx=pxx + (dex(l)*dex(l) - cssq)*fneq
			pyy=pyy + (dey(l)*dey(l) - cssq)*fneq
			pxy=pxy +  dex(l)*dey(l)*fneq
          enddo
          do l=0,8
            fpop(l,ic,i,j,flip)= feq(l) + ((0.5d0*p(l))/(cssq**2.d0))*((dex(l)*dex(l) - cssq)*pxx + &
		     (dey(l)*dey(l) - cssq)*pyy+2.0d0*dex(l)*dey(l)*pxy)
          enddo
        endif
      enddo
    enddo
	!$omp end do nowait
    endsubroutine
    !**********************************************************!
    subroutine collision
        implicit none
        integer :: mm,i,j,ic,flip
        real*8,dimension(0:8) :: feq
        real*8 :: uu,udotc
        
        flip=mod(step-1,2)+1
        if(ncomp.ne.1)then
          write(6,*)'ncomp==2 not written for collision'
          stop
        endif
        ic=1
        !$omp do schedule(static,mychunk) private(j)
        do j=1,ny
            do i=1,nx
                if(isfluid_LB(i,j).eq.1)then
                    uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
                    do mm=0,8
                        udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
                        ! page eq 3.4 pag 61 kruger
                        feq(mm)=p(mm)*rho(ic,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
                        ! page eq 3.8 pag 66 kruger
                        fpop(mm,ic,i,j,flip)=fpop(mm,ic,i,j,flip) + omega*(feq(mm) - fpop(mm,ic,i,j,flip))   
                    enddo
                endif
            enddo
        enddo
        !$omp end do nowait
    endsubroutine
    
    subroutine collision_2c
        implicit none
        integer :: mm,ll,i,j,flip
        real*8,dimension(2,0:8) :: feq
        real*8 :: uu,udotc,nu_avg
        
        flip=mod(step-1,2)+1
        !$omp do schedule(static,mychunk) private(j) 
        do j=1,ny
            do i=1,nx
                if(isfluid_LB(i,j).eq.1)then
                    uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
                   ! nu_avg=1.0d0/( (rho(1,i,j)/(rho(1,i,j)+ rho(2,i,j)))*one_ov_nuR + & 
                   !        (rho(2,i,j)/(rho(1,i,j)+ rho(2,i,j)))*one_ov_nuB)
                   ! omega_c(i,j)=2.0d0/(6.0d0*nu_avg + 1.0d0)
                    do mm=0,8
                        udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
                        feq(1,mm)=rho(1,i,j)*(myphi(mm,1)+p(mm)*( udotc + 0.5d0*udotc*udotc - uu))
                        feq(2,mm)=rho(2,i,j)*(myphi(mm,2)+p(mm)*(udotc + 0.5d0*udotc*udotc - uu))
                        fpop(mm,1,i,j,flip)=fpop(mm,1,i,j,flip) + tau*(feq(1,mm) - fpop(mm,1,i,j,flip))  
                        fpop(mm,2,i,j,flip)=fpop(mm,2,i,j,flip) + tau*(feq(2,mm) - fpop(mm,2,i,j,flip))
                    enddo
                endif
            enddo
        enddo
        !$omp end do
    endsubroutine
    
    subroutine compute_grads
      implicit none
      integer :: mm,ll,i,j,ic,flip,ii,jj
      real*8,dimension(0:8) :: myrhoR,myrhoB,uxx,uyy
#ifndef HUANG
      return
#endif      
      
      flip=mod(step-1,2)+1
      !$omp single
      !west pbc
      uxy(1:2,0,1:ny)=uxy(1:2,nx,1:ny)
      !east pbc
      uxy(1:2,nx+1,1:ny)=uxy(1:2,1,1:ny)
      !$omp end single
      !$omp barrier
      
      if(ncomp==2)then
        !eq 6.16 pag 237 kruger
        !$omp do schedule(static,mychunk) private(j)
        do j=1,ny
          do i=1,nx
            gradrhoR(1:2,i,j)=0.d0
		    gradrhoB(1:2,i,j)=0.d0
		    gradrhovR(1:2,i,j)=0.d0
		    gradrhovB(1:2,i,j)=0.d0
            if(isfluid_LB(i,j).eq.1)then  
              do mm=0,8
                ii=i+ex(mm)
!                if(ii>nx)ii=ii-nx
!                if(ii<1)ii=ii+nx
                jj=j+ey(mm)
			    myrhoR(mm)=rho(1,ii,jj)
			    myrhoB(mm)=rho(2,ii,jj)
			    uxx(mm)=uxy(1,ii,jj)
			    uyy(mm)=uxy(2,ii,jj)
		      enddo
		      do mm=0,8
		        gradrhoR(1,i,j)=gradrhoR(1,i,j)+(p(mm)/cssq)*dex(mm)*myrhoR(mm)
                gradrhoR(2,i,j)=gradrhoR(2,i,j)+(p(mm)/cssq)*dey(mm)*myrhoR(mm)
                gradrhoB(1,i,j)=gradrhoB(1,i,j)+(p(mm)/cssq)*dex(mm)*myrhoB(mm)
                gradrhoB(2,i,j)=gradrhoB(2,i,j)+(p(mm)/cssq)*dey(mm)*myrhoB(mm)
                
                gradrhovR(1,i,j)=gradrhovR(1,i,j)+(p(mm)/cssq)*dex(mm)*myrhoR(mm)*uxx(mm)
                gradrhovR(2,i,j)=gradrhovR(2,i,j)+(p(mm)/cssq)*dey(mm)*myrhoR(mm)*uyy(mm)
                gradrhovB(1,i,j)=gradrhovB(1,i,j)+(p(mm)/cssq)*dex(mm)*myrhoB(mm)*uxx(mm)
                gradrhovB(2,i,j)=gradrhovB(2,i,j)+(p(mm)/cssq)*dey(mm)*myrhoB(mm)*uyy(mm)
              enddo
            endif
          enddo
        enddo
        !$omp end do
        
        !$omp single
        !west pbc
        gradrhoR(1:2,0,1:ny)=gradrhoR(1:2,nx,1:ny)
        gradrhoB(1:2,0,1:ny)=gradrhoB(1:2,nx,1:ny)
        gradrhovR(1:2,0,1:ny)=gradrhovR(1:2,nx,1:ny)
        gradrhovB(1:2,0,1:ny)=gradrhovB(1:2,nx,1:ny)
        !east pbc
        gradrhoR(1:2,nx+1,1:ny)=gradrhoR(1:2,1,1:ny)
        gradrhoB(1:2,nx+1,1:ny)=gradrhoB(1:2,1,1:ny)
        gradrhovR(1:2,nx+1,1:ny)=gradrhovR(1:2,1,1:ny)
        gradrhovB(1:2,nx+1,1:ny)=gradrhovB(1:2,1,1:ny)
        !$omp end single
        !$omp barrier
        
      endif    
        
    end subroutine compute_grads
    
!    subroutine collision_EDM
!        implicit none
!        integer :: mm,ll,i,j,ic,flip,ii,jj
!        real*8,dimension(0:8) :: freg,feq,feq_shifted,fneqreg,myrhoR,myrhoB
!        real*8,dimension(1:5) :: arrfive
!        real*8 :: uu,uu_shifted,udotc,udotc_shifted,uxy_shifted(2),myomega, &
!         totrho,myrho,myi
!        real*8 :: pxx,pyy,pxy,fneq,mystress(2,2),modstress,tau_temp,modshear, &
!         mykinvisc,newtau_temp,myvel(2),phase_field, &
!         onevisc_ave,visc_ave,tau_ave,viscR,viscB,psi_R(2),psi_B(2),u_R(2),u_B(2)
!        real*8,dimension(2,2) :: mat_R,mat_B,mat_TR,mat_TB,myG_R,myG_B
!        real*8,dimension(1:ncomp,0:8) :: corr_c
        
!      flip=mod(step-1,2)+1
!      if(ncomp==2)then
!        !eq 6.16 pag 237 kruger
!        !$omp do schedule(static,mychunk) private(j)
!        do j=1,ny
!            do i=1,nx
!                if(isfluid_LB(i,j).eq.1)then
!                    ic=1  !first component
!                    pxx=0.0d0
!		            pyy=0.0d0
!		            pxy=0.0d0
                    
!		            myvel(1:2)=uxy(1:2,i,j)
		            
!		            do mm=0,8
!                        ii=i+ex(mm)
!!                        if(ii>nx)ii=ii-nx
!!                        if(ii<1)ii=ii+nx
!                        jj=j+ey(mm)
!					    myrhoR(mm)=rho(1,ii,jj)
!					    myrhoB(mm)=rho(2,ii,jj)
!		            enddo
!		            phase_field= (myrhoR(0)/mydens-myrhoB(0)/mydensB)/ &
!					   (myrhoR(0)/mydens+myrhoB(0)/mydensB)
!		            totrho=rho(1,i,j)+rho(2,i,j)
!#ifdef CGCORR
!#ifdef HUANG
!                      u_R(1)=0.d0
!                      u_R(2)=0.d0
!                      do mm=0,8
!                        ii=i+ex(mm)
!                        jj=j+ey(mm)
!					    u_R(1)=u_R(1)+(p(mm)/cssq)*dex(mm)*(uxy(1,ii,jj)*gradrhoR(1,ii,jj)+&
!					     uxy(1,ii,jj)*gradrhoR(1,ii,jj)+gradrhovR(1,ii,jj)+gradrhovR(2,ii,jj))+ &
!					    (p(mm)/cssq)*dey(mm)*(uxy(2,ii,jj)*gradrhoR(1,ii,jj)+&
!					     uxy(1,ii,jj)*gradrhovR(2,ii,jj))
					     
!					    u_R(2)=u_R(2)+(p(mm)/cssq)*dex(mm)*(uxy(1,ii,jj)*gradrhoR(2,ii,jj)+&
!					     uxy(2,ii,jj)*gradrhoR(1,ii,jj))+ &
!					    (p(mm)/cssq)*dey(mm)*(uxy(2,ii,jj)*gradrhoR(2,ii,jj)+&
!					     uxy(2,ii,jj)*gradrhovR(2,ii,jj)+gradrhovR(1,ii,jj)+gradrhovR(2,ii,jj))
!		              enddo
		              
!!		              if(step==1000 .and. dabs(phase_field)<=0.5d0)then
!!		                write(6,*)i,j,u_R
!!		                stop
!!		              endif
		              
!		              u_B(1)=0.d0
!                      u_B(2)=0.d0
!                      do mm=0,8
!                        ii=i+ex(mm)
!                        jj=j+ey(mm)
!					    u_B(1)=u_B(1)+(p(mm)/cssq)*dex(mm)*(uxy(1,ii,jj)*gradrhoB(1,ii,jj)+&
!					     uxy(1,ii,jj)*gradrhoB(1,ii,jj)+gradrhovB(1,ii,jj)+gradrhovB(2,ii,jj))+ &
!					    (p(mm)/cssq)*dey(mm)*(uxy(2,ii,jj)*gradrhoB(1,ii,jj)+&
!					     uxy(1,ii,jj)*gradrhovB(2,ii,jj))
					     
!					    u_B(2)=u_B(2)+(p(mm)/cssq)*dex(mm)*(uxy(1,ii,jj)*gradrhoB(2,ii,jj)+&
!					     uxy(2,ii,jj)*gradrhoB(1,ii,jj))+ &
!					    (p(mm)/cssq)*dey(mm)*(uxy(2,ii,jj)*gradrhoB(2,ii,jj)+&
!					     uxy(2,ii,jj)*gradrhovB(2,ii,jj)+gradrhovB(1,ii,jj)+gradrhovB(2,ii,jj))
!		              enddo
		              
!                      u_R(1:2)=((consistency/totrho)/cssq)*(cssq-cssqR)*(-u_R(1:2))
!                      u_B(1:2)=(visc_LB/cssq)*(cssq-cssqB)*(-u_B(1:2))
!                      huangR(1:2,i,j)=u_R(1:2)
!                      huangB(1:2,i,j)=u_B(1:2)
                      
!                      do mm=0,8
!                        corr_c(1,0:8)=(p(mm)/cssq)*(u_R(1)*dex(mm)+u_R(2)*dey(mm))
!                        corr_c(2,0:8)=(p(mm)/cssq)*(u_B(1)*dex(mm)+u_B(2)*dey(mm))
!                      enddo
!#else
!                      psi_R(1:2)=0.0d0
!					  psi_B(1:2)=0.0d0
!					  !
!					  do mm=1,8
!						psi_R(1)=psi_R(1)+ (p(mm)/cssq)*dex(mm)*myrhoR(mm)
!						psi_R(2)=psi_R(2)+ (p(mm)/cssq)*dey(mm)*myrhoR(mm)
!					  enddo
!					  do mm=1,8
!						psi_B(1)=psi_B(1)+ (p(mm)/cssq)*dex(mm)*myrhoB(mm)
!						psi_B(2)=psi_B(2)+ (p(mm)/cssq)*dey(mm)*myrhoB(mm)
!					  enddo
					  
!					  mat_R=tensor_product(myvel,psi_R)
!					  mat_B=tensor_product(myvel,psi_B)
!					  mat_TR=transpose(mat_R)
!					  mat_TB=transpose(mat_B)
!					  myG_R=(1.d0/8.d0)*(mat_R+mat_TR)
!					  myG_B=(1.d0/8.d0)*(mat_B+mat_TB)
					  
!					  corr_c(1,0)=-3.d0*visc_ave*dot_product(myvel,psi_R)
!					  corr_c(2,0)=-3.d0*visc_ave*dot_product(myvel,psi_B)
!					  do mm=1,4
!					    corr_c(1,mm)=4.d0*visc_ave*tensor_contr_tr2(myG_R,cmat,ndir,mm)
!					    corr_c(2,mm)=4.d0*visc_ave*tensor_contr_tr2(myG_B,cmat,ndir,mm)
!					  enddo
!					  do mm=5,8
!					    corr_c(1,mm)=1.d0*visc_ave*tensor_contr_tr2(myG_R,cmat,ndir,mm)
!					    corr_c(2,mm)=1.d0*visc_ave*tensor_contr_tr2(myG_B,cmat,ndir,mm)
!					  enddo
!#endif
!#else
!                      corr_c(1:ncomp,0:8)=0.d0
!#endif
!                    !comute equilibrium
!		            uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
!#ifdef NEWEQ
!                    do mm=0,8
!		              udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq	
!		              ! page eq 3.4 pag 61 kruger	
!!		              feq(mm)=myrhoR(0)*(myphi(mm,1) + p(mm)*( udotc* &
!!		               (1.d0+0.5d0*(cssqR/cssq-1.d0)* &
!!		               ((dex(mm)*dex(mm)+dey(mm)*dey(mm))/cssq-4.d0) &
!!		               + 0.5d0*udotc*udotc - uu))) &
!!		               +corr_c(1,mm) !4.5 of the book
		              
!		              feq(mm)=myrhoR(0)*(myphi(mm,1) + p(mm)*( udotc + 0.5d0*udotc*udotc - uu)&
!		               +0.5d0*udotc*(cssqR/cssq-1.d0)*((dex(mm)*dex(mm)+dey(mm)*dey(mm))/cssq-5.d0))
		               
!		            enddo
!#else
!		            do mm=0,8
!		              udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq	
!		              ! page eq 3.4 pag 61 kruger	
!		              feq(mm)=myrhoR(0)*(myphi(mm,1) + p(mm)*( udotc + 0.5d0*udotc*udotc - uu))&
!		               +corr_c(1,mm) !4.5 of the book
!		            enddo
!#endif

!					!!!!start of regularization	
!						!i am using f(1)=fneq first part of Eq4 of Mathematics and Computers in Simulation 72 (2006) 165168
!                    do mm=0,8
!                    ! i am already mixing using first parto of Eq 5 of Mathematics and Computers in Simulation 72 (2006) 165168
!                      fneq= fpop(mm,ic,i,j,flip) - feq(mm)
!                      pxx=pxx + (dex(mm)*dex(mm))*fneq
!			          pyy=pyy + (dey(mm)*dey(mm))*fneq
!			          pxy=pxy +  dex(mm)*dey(mm)*fneq
!                    enddo
!                    do mm=0,8
!                    !eq6 of Mathematics and Computers in Simulation 72 (2006) 165168
!                      fneqreg(mm) = ((0.5d0*p(mm))/(cssq**2.d0))*((dex(mm)*dex(mm) - cssq)*pxx + &
!		               (dey(mm)*dey(mm) - cssq)*pyy+2.0d0*dex(mm)*dey(mm)*pxy)
!#ifdef REGULARIZED
!                      freg(mm)= feq(mm) + fneqreg(mm)
!#else
!                      freg(mm)=fpop(mm,ic,i,j,flip)
!#endif
!                    enddo
!                    !!!!end of regularization
                 
!                    !the hugly part of non-newtonian
!                    !first comute the stress tensor
!                    !Eq 21 of PHYSICAL REVIEW E 79, 046704 2009 !using fneqreg instead of fneq
!                    mystress(1:2,1:2)=0.d0   
!                    do mm=0,8
!                      mystress(1,1)=mystress(1,1)+(dex(mm)*dex(mm)-0.5d0)*fneqreg(mm)
!                      mystress(1,2)=mystress(1,2)+dex(mm)*dey(mm)*fneqreg(mm)
!                      mystress(2,1)=mystress(2,1)+dey(mm)*dex(mm)*fneqreg(mm)
!                      mystress(2,2)=mystress(2,2)+(dey(mm)*dey(mm)-0.5d0)*fneqreg(mm)
!                    enddo
!#ifdef CORRTENS
!                    !EQ 26 Lattice Boltzmann modeling of the capillary rise of non-Newtonian power-law fluids
!                    !EQ 20 Regularized lattice Boltzmann model for immiscible two-phase flows with power-law rheology
!                    mystress(1,1)=mystress(1,1)+uxy(1,i,j)*fx_LB
!                    mystress(1,2)=mystress(1,2)+0.5d0*(uxy(1,i,j)*fy_LB+uxy(2,i,j)*fx_LB)
!                    mystress(2,1)=mystress(2,1)+0.5d0*(uxy(1,i,j)*fy_LB+uxy(2,i,j)*fx_LB)
!                    mystress(2,2)=mystress(2,2)+uxy(2,i,j)*fy_LB
!#endif
!                    !EQ 25 Lattice Boltzmann modeling of the capillary rise of non-Newtonian power-law fluids
!                    modstress=dsqrt(2.d0*(mystress(1,1)**2.d0+mystress(1,2)**2.d0+ &
!                      mystress(2,1)**2.d0+mystress(2,2)**2.d0))
!                    tau_temp=(consistency/totrho)/cssq+0.5d0 !ansat
!                    if(flowindex.ne.1.d0)then   !check if I am not newtonian
!                    !
!                      modshear=modstress/(2.d0*totrho*tau_temp*cssq)
!                      if(modshear<=zeroshear)then
!                        mykinvisc=maxvisc/totrho
!                      elseif(modshear>=maxshear)then
!                          mykinvisc=minvisc/totrho
!                      else
!                        mykinvisc=(consistency*(modshear**(flowindex-1.d0))+yieldst/modshear)/totrho
!                      endif
!                      newtau_temp=mykinvisc/cssq+0.5d0
                      
!                      do while(abs(newtau_temp-tau_temp).gt.convlimit)
!                        tau_temp=newtau_temp
!                        modshear=modstress/(2.d0*totrho*tau_temp*cssq)
!                        if(modshear<=zeroshear)then
!                          mykinvisc=maxvisc/totrho
!                        elseif(modshear>=maxshear)then
!                          mykinvisc=minvisc/totrho
!                        else
!                          mykinvisc=(consistency*(modshear**(flowindex-1.d0))+yieldst/modshear)/totrho
!                        endif
!                        newtau_temp=mykinvisc/cssq+0.5d0
!                      enddo
!                    else
!                      newtau_temp=tau_temp   
!                    endif
                    
!                    viscR=cssq*(newtau_temp-0.5d0) 
!                    viscB=visc_LB
!                    taufield(i,j)=newtau_temp
!                    myomega=1.0d0/newtau_temp 
!                    if(dabs(newtau_temp-0.5d0)<1.d-4)then
!                      write(6,*)'errore in compute_omegarel! tao piccolo = ',newtau_temp
!                      stop
!                    endif
                      
                    
!                    !onevisc_ave=myrhoR(0)/totrho*(1.d0/viscR)+ &
!                    ! myrhoB(0)/totrho*(1.d0/viscB) !eq 13 PHYSICAL REVIEW E 95, 033306 (2017)
!                    !visc_ave=1.d0/onevisc_ave
                    
!                    myi=dble(j-hlength)-0.5d0

!                    if(dabs(myi)>hhlengthref)then
!                      visc_ave=viscR
!                    else
!                      visc_ave=viscB
!                    endif
!                    visc_ave=aveq(myrhoR(0),myrhoB(0),viscR,viscB,phase_field,0.05d0,-2)
!                    tau_ave=visc_ave/cssq+0.5d0
!                    omega_c(i,j)=1.d0/tau_ave !eq 14 PHYSICAL REVIEW E 95, 033306 (2017)
!                    myomega=omega_c(i,j)
                    
!                    uxy_shifted(1)=uxy(1,i,j)+fx_LB/totrho
!                    uxy_shifted(2)=uxy(2,i,j)+fy_LB/totrho
!                    uu_shifted=0.5d0*(uxy_shifted(1)*uxy_shifted(1) + &
!                     uxy_shifted(2)*uxy_shifted(2))/cssq


	                  
	                  
!					  do mm=0,8
!                        udotc_shifted=(uxy_shifted(1)*dex(mm) + uxy_shifted(2)*dey(mm))/cssq
!                        ! page eq 3.4 pag 61 kruger
!                        feq_shifted(mm)=myrhoR(0)* &
!                         (myphi(mm,1) + p(mm)*(udotc_shifted + 0.5d0*udotc_shifted*udotc_shifted - uu_shifted)) &
!                          + corr_c(1,mm)
!                        ! page eq 3.8 pag 66 kruger + kupershtohk eq 6.32 pag 243 kruger
!                        fpop(mm,ic,i,j,flip)=fpop(mm,ic,i,j,flip) + myomega*(feq(mm) - fpop(mm,ic,i,j,flip)) + &
!                       ! fpop(mm,ic,i,j,flip)=freg(mm) + myomega*(feq(mm) - freg(mm)) + &
!                         !(p(mm)/cssq)*(fx_LB*dex(mm)+fy_LB*dey(mm))
!                         (feq_shifted(mm) -feq(mm)) 
!                    enddo
					  
!!					  do mm=0,26 
!!						udotc=(myvel(1)*dex(mm) + myvel(2)*dey(mm) + myvel(3)*dez(mm))/cssq
!!						feq_c(1,mm)=myrhoR(0)*(phiCG(1,mm)+p(mm)*(udotc+0.5d0*udotc*udotc-uu)) &
!!						 + corr_c(1,mm)
!!		 	            feq_c(2,mm)=myrhoB(0)*(phiCG(2,mm)+p(mm)*(udotc+0.5d0*udotc*udotc-uu)) &
!!		 	             + corr_c(2,mm)
!!						fpop(1,mm,i,j,k,flip)=uno_omega*fpop(1,mm,i,j,k,flip) + omega*feq_c(1,mm)  
!!						fpop(2,mm,i,j,k,flip)=uno_omega*fpop(2,mm,i,j,k,flip) + omega*feq_c(2,mm)
!!					  enddo

!                    !!!!!!!second component!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                    ic=2
!                    pxx=0.0d0
!		            pyy=0.0d0
!		            pxy=0.0d0
!                        !comute equilibrium
!		            uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
!#ifdef NEWEQ
!                    do mm=0,8
!		              udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq	
!		              ! page eq 3.4 pag 61 kruger	
!!		              feq(mm)=myrhoB(0)*(myphi(mm,2) + p(mm)*( udotc* &
!!		               (1.d0+0.5d0*(cssqB/cssq-1.d0)* &
!!		               ((dex(mm)*dex(mm)+dey(mm)*dey(mm))/cssq-4.d0) &
!!		               + 0.5d0*udotc*udotc - uu)))&
!!		               +corr_c(2,mm) !4.5 of the book
!		              feq(mm)=myrhoR(0)*(myphi(mm,2) + p(mm)*( udotc + 0.5d0*udotc*udotc - uu)&
!		               +0.5d0*udotc*(cssqB/cssq-1.d0)*((dex(mm)*dex(mm)+dey(mm)*dey(mm))/cssq-5.d0))
!		            enddo
!#else
!		            do mm=0,8
!		              udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq	
!		              ! page eq 3.4 pag 61 kruger	
!		              feq(mm)=myrhoB(0)*(myphi(mm,2) + p(mm)*( udotc + 0.5d0*udotc*udotc - uu))&
!		               +corr_c(2,mm) 
!		            enddo
!#endif

!					!!!!start of regularization	
!						!i am using f(1)=fneq first part of Eq4 of Mathematics and Computers in Simulation 72 (2006) 165168
!                    do mm=0,8
!                    ! i am already mixing using first parto of Eq 5 of Mathematics and Computers in Simulation 72 (2006) 165168
!                      fneq= fpop(mm,ic,i,j,flip) - feq(mm)
!                      pxx=pxx + (dex(mm)*dex(mm))*fneq
!			          pyy=pyy + (dey(mm)*dey(mm))*fneq
!			          pxy=pxy +  dex(mm)*dey(mm)*fneq
!                    enddo
!                    do mm=0,8
!                    !eq6 of Mathematics and Computers in Simulation 72 (2006) 165168
!                      fneqreg(mm) = ((0.5d0*p(mm))/(cssq**2.d0))*((dex(mm)*dex(mm) - cssq)*pxx + &
!		               (dey(mm)*dey(mm) - cssq)*pyy+2.0d0*dex(mm)*dey(mm)*pxy)
!#ifdef REGULARIZED
!                      freg(mm)= feq(mm) + fneqreg(mm)
!#else
!                      freg(mm)=fpop(mm,ic,i,j,flip)
!#endif
!                    enddo
!                    !!!!end of regularization
                  
!                    !the hugly part of non-newtonian
!                    !first comute the stress tensor
!                    !Eq 21 of PHYSICAL REVIEW E 79, 046704 2009 !using fneqreg instead of fneq
!!                    mystress(1:2,1:2)=0.d0   
!!                    do mm=0,8
!!                      mystress(1,1)=mystress(1,1)+(dex(mm)*dex(mm)-0.5d0)*fneqreg(mm)
!!                      mystress(1,2)=mystress(1,2)+dex(mm)*dey(mm)*fneqreg(mm)
!!                      mystress(2,1)=mystress(2,1)+dey(mm)*dex(mm)*fneqreg(mm)
!!                      mystress(2,2)=mystress(2,2)+(dey(mm)*dey(mm)-0.5d0)*fneqreg(mm)
!!                    enddo
!!                    modstress=dsqrt(mystress(1,1)**2.d0+mystress(1,2)**2.d0+ &
!!                      mystress(2,1)**2.d0+mystress(2,2)**2.d0)
!                    !tau_temp=visc_LB/cssq+0.5d0 !fixed by input
!                    !newtau_temp=tau_temp  
                    
!                    !myomega=1.0d0/tau_temp !newtau_temp 
!                    !myomega=omega_c(i,j)
!                    if(dabs(newtau_temp-0.5d0)<1.d-4)then
!                      write(6,*)'errore in compute_omegarel! tao piccolo = ',newtau_temp
!                      stop
!                    endif      
!                    uxy_shifted(1)=uxy(1,i,j)+fx_LB/totrho
!                    uxy_shifted(2)=uxy(2,i,j)+fy_LB/totrho
!                    uu_shifted=0.5d0*(uxy_shifted(1)*uxy_shifted(1) + &
!                    uxy_shifted(2)*uxy_shifted(2))/cssq

!                    do mm=0,8
!                        udotc_shifted=(uxy_shifted(1)*dex(mm) + uxy_shifted(2)*dey(mm))/cssq
!                        ! page eq 3.4 pag 61 kruger
!                        feq_shifted(mm)=myrhoB(0)* &
!                         (myphi(mm,2) + p(mm)*(udotc_shifted + 0.5d0*udotc_shifted*udotc_shifted - uu_shifted))&
!                         +corr_c(2,mm) 
!                        ! page eq 3.8 pag 66 kruger + kupershtohk eq 6.32 pag 243 kruger
!                         fpop(mm,ic,i,j,flip)=fpop(mm,ic,i,j,flip) + myomega*(feq(mm) - fpop(mm,ic,i,j,flip)) +&
!                        !fpop(mm,ic,i,j,flip)=freg(mm) + myomega*(feq(mm) - freg(mm)) + &
!                         !(p(mm)/cssq)*(fx_LB*dex(mm)+fy_LB*dey(mm))
!                         (feq_shifted(mm) -feq(mm)) 
!                    enddo                   
!                endif
!            enddo
!        enddo
!        !$omp end do
!      else
!        ic=1
!        !eq 6.16 pag 237 kruger
!        !$omp do schedule(static,mychunk) private(j)
!        do j=1,ny
!            do i=1,nx
!                if(isfluid_LB(i,j).eq.1)then
                
!                    pxx=0.0d0
!		            pyy=0.0d0
!		            pxy=0.0d0
!                        !comute equilibrium
!		            uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
!		            myrho=rho(ic,i,j)
!		            do mm=0,8
!		              udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq	
!		              ! page eq 3.4 pag 61 kruger	
!		              feq(mm)=p(mm)*myrho*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
!		            enddo
!#ifdef REGULARIZED 

!					!!!!start of regularization	
!				    !i am using f(1)=fneq first part of Eq4 of Mathematics and Computers in Simulation 72 (2006) 165168
!                    do mm=0,8
!                    ! i am already mixing using first parto of Eq 5 of Mathematics and Computers in Simulation 72 (2006) 165168
!                      fneq= fpop(mm,ic,i,j,flip) - feq(mm)
!                      pxx=pxx + (dex(mm)*dex(mm) - cssq)*fneq
!			          pyy=pyy + (dey(mm)*dey(mm) - cssq)*fneq
!			          pxy=pxy +  dex(mm)*dey(mm)*fneq
!                    enddo
!                    do mm=0,8
!                    !eq6 of Mathematics and Computers in Simulation 72 (2006) 165168
!                      fneqreg(mm) = ((0.5d0*p(mm))/(cssq**2.d0))*((dex(mm)*dex(mm) - cssq)*pxx + &
!		               (dey(mm)*dey(mm) - cssq)*pyy+2.0d0*dex(mm)*dey(mm)*pxy)
!                      freg(mm)= feq(mm) + fneqreg(mm)
!                    enddo
!                    !!!!end of regularization

!                    !the hugly part of non-newtonian
!                    !first comute the stress tensor
!                    !Eq 21 of PHYSICAL REVIEW E 79, 046704 2009 !using fneqreg instead of fneq
!                    !compute strain rate tensor epsilon
!                    mystress(1:2,1:2)=0.d0  
!                    do mm=0,8
!                      mystress(1,1)=mystress(1,1)+(dex(mm)*dex(mm))*fneqreg(mm)
!                      mystress(1,2)=mystress(1,2)+dex(mm)*dey(mm)*fneqreg(mm)
!                      mystress(2,1)=mystress(2,1)+dey(mm)*dex(mm)*fneqreg(mm)
!                      mystress(2,2)=mystress(2,2)+(dey(mm)*dey(mm))*fneqreg(mm)
!                    enddo
!#else
!                    mystress(1:2,1:2)=0.d0  
!                    do mm=0,8
!                      mystress(1,1)=mystress(1,1)+(dex(mm)*dex(mm))*(fpop(mm,ic,i,j,flip)-feq(mm))
!                      mystress(1,2)=mystress(1,2)+dex(mm)*dey(mm)*(fpop(mm,ic,i,j,flip)-feq(mm))
!                      mystress(2,1)=mystress(2,1)+dey(mm)*dex(mm)*(fpop(mm,ic,i,j,flip)-feq(mm))
!                      mystress(2,2)=mystress(2,2)+(dey(mm)*dey(mm))*(fpop(mm,ic,i,j,flip)-feq(mm))
!                    enddo
!#endif 
!#ifdef CORRTENS
!                    !EQ 26 Lattice Boltzmann modeling of the capillary rise of non-Newtonian power-law fluids
!                    !EQ 20 Regularized lattice Boltzmann model for immiscible two-phase flows with power-law rheology
!                    mystress(1,1)=mystress(1,1)+uxy(1,i,j)*fx_LB
!                    mystress(1,2)=mystress(1,2)+0.5d0*(uxy(1,i,j)*fy_LB+uxy(2,i,j)*fx_LB)
!                    mystress(2,1)=mystress(2,1)+0.5d0*(uxy(1,i,j)*fy_LB+uxy(2,i,j)*fx_LB)
!                    mystress(2,2)=mystress(2,2)+uxy(2,i,j)*fy_LB
!#endif
!                    !EQ 25 Lattice Boltzmann modeling of the capillary rise of non-Newtonian power-law fluids
!                    modstress=dsqrt(2.d0*(mystress(1,1)**2.d0+mystress(1,2)**2.d0+ &
!                      mystress(2,1)**2.d0+mystress(2,2)**2.d0))
!                    tau_temp=visc_LB/cssq+0.5d0 !ansat
!                    newtau_temp=tau_temp  
!                    myi=dble(j-hlength)-0.5d0 
!                    if(lfaketwo .and. dabs(myi)>hhlengthref)goto 130
!                    if(flowindex.ne.1.d0)then   !check if I am not newtonian
!                    !
!                      modshear=modstress/(2.d0*myrho*tau_temp*cssq)
!                      if(modshear<=zeroshear)then
!                        mykinvisc=maxvisc/myrho
!                      elseif(modshear>=maxshear)then
!                          mykinvisc=minvisc/myrho
!                      else
!                        mykinvisc=(consistency*(modshear**(flowindex-1.d0))+yieldst/modshear)/myrho
!                      endif
!                      newtau_temp=mykinvisc/cssq+0.5d0
                      
!                      do while(abs(newtau_temp-tau_temp).gt.convlimit)
!                        tau_temp=newtau_temp
!                        modshear=modstress/(2.d0*myrho*tau_temp*cssq)
!                        if(modshear<=zeroshear)then
!                          mykinvisc=maxvisc/myrho
!                        elseif(modshear>=maxshear)then
!                          mykinvisc=minvisc/myrho
!                        else
!                          mykinvisc=(consistency*(modshear**(flowindex-1.d0))+yieldst/modshear)/myrho
!                        endif
!                        newtau_temp=mykinvisc/cssq+0.5d0
!                      enddo
!                    else
!                      newtau_temp=tau_temp   
!                    endif  
!130                 continue    
!                    taufield(i,j)=newtau_temp
!                    myomega=1.0d0/newtau_temp 
!                    if(dabs(newtau_temp-0.5d0)<1.d-4)then
!                      write(6,*)'errore in compute_omegarel! tao piccolo = ',newtau_temp
!                      stop
!                    endif      
!                    uxy_shifted(1)=uxy(1,i,j)+fx_LB/myrho
!                    uxy_shifted(2)=uxy(2,i,j)+fy_LB/myrho
!                    uu_shifted=0.5d0*(uxy_shifted(1)*uxy_shifted(1) + &
!                     uxy_shifted(2)*uxy_shifted(2))/cssq
!                    do mm=0,8
!                        udotc_shifted=(uxy_shifted(1)*dex(mm) + uxy_shifted(2)*dey(mm))/cssq
!                        ! page eq 3.4 pag 61 kruger
!                        feq_shifted(mm)=p(mm)*myrho* &
!                         (1.0d0 + udotc_shifted + 0.5d0*udotc_shifted*udotc_shifted - uu_shifted)
!                        ! page eq 3.8 pag 66 kruger + kupershtohk eq 6.32 pag 243 kruger
!#ifdef REGULARIZED 
!                        fpop(mm,ic,i,j,flip)=freg(mm) + myomega*(feq(mm) - freg(mm)) + &
!                         (feq_shifted(mm) -feq(mm)) 
!#else
!                        fpop(mm,ic,i,j,flip)= &
!                         fpop(mm,ic,i,j,flip)+myomega*(feq(mm)-fpop(mm,ic,i,j,flip))+ &
!                         (feq_shifted(mm) -feq(mm)) 
!#endif
!                    enddo
!                endif
!            enddo
!        enddo
!        !$omp end do
!      endif
!    end subroutine collision_EDM
    
    subroutine collision_EDM_NW
        implicit none
        integer :: mm,ll,i,j,ic,flip,ii,jj
        real*8,dimension(0:8) :: freg,feq,feq_shifted,fneqreg,myrhoR,myrhoB
        real*8,dimension(1:5) :: arrfive
        real*8 :: uu,uu_shifted,udotc,udotc_shifted,uxy_shifted(2),myomega, &
         totrho,myrho,myi,phase_temp,psix,psiy,psinorm_sq,psinorm,ckl_RB
        real*8 :: pxx,pyy,pxy,fneq,mystress(2,2),modstress,tau_temp,modshear, &
         mykinvisc,newtau_temp,myvel(2),phase_field,alpha_ave, &
         onevisc_ave,visc_ave,tau_ave,viscR,viscB,psi_dens(2),u_R(2),u_B(2)
        real*8 :: acoeff,cosphi,temp,dot_vel_psi
        real*8,dimension(2,2) :: mat_dens,mat_Tdens,myG,cmattemp
        real*8,dimension(0:8) :: corr_c,e_dot_psi
        
      flip=mod(step-1,2)+1
      if(ncomp==2)then
        !eq 6.16 pag 237 kruger
        !$omp do schedule(static,mychunk) private(j)
        do j=1,ny
            do i=1,nx
                if(isfluid_LB(i,j).eq.1)then
                    ic=1  !first component
                    
                    
		            myvel(1:2)=uxy(1:2,i,j)
		            
		            viscR=visc_LB_R
                    viscB=visc_LB
                      

		            
		            do mm=0,8
                        ii=i+ex(mm)
                        jj=j+ey(mm)
                        if(lpbc(1))then
                          !pbc along x
                          if(ii<1)ii=ii+nx
                          if(ii>nx)ii=ii-nx
                        endif
                        if(lpbc(2))then
                          !pbc along x
                          if(jj<1)jj=jj+ny
                          if(jj>ny)jj=jj-ny
                        endif
                        !I check if I am in the grid and not outsude
                        if(lpbc(1))then
                          if(ii<0 .or. ii>nx)then
                            write(6,*)'ERRORE collision_EDM_NW1'
                            stop
                          endif
                        else
                          if(ii<0 .or. ii>nx)cycle
                        endif
                        if(lpbc(2))then
                          if(jj<0 .or. jj>ny)then
                            write(6,*)'ERRORE collision_EDM_NW2'
                            stop
                          endif
                        else
                          if(jj<0 .or. jj>ny)cycle
                        endif
					    myrhoR(mm)=rho(1,ii,jj)
					    myrhoB(mm)=rho(2,ii,jj)
		            enddo
		            phase_field= (myrhoR(0)/mydensR-myrhoB(0)/mydensB)/ &
					   (myrhoR(0)/mydensR+myrhoB(0)/mydensB)
					   
					!phase_field= (myrhoR(0)-myrhoB(0))/ &
					!   (myrhoR(0)+myrhoB(0))   
					   
					   
		            totrho=rho(1,i,j)+rho(2,i,j)
		            
		            !visc_ave=aveq(myrhoR(0),myrhoB(0),viscR,viscB,phase_field,0.05d0,-2)
                    !tau_ave=visc_ave/cssq+0.5d0
                    tau_ave=0.5d0*(1.d0+phase_field)*tau_R+0.5d0*(1.d0-phase_field)*tau_B
                    visc_ave = cssq*(tau_ave-0.5d0)
                    myomega=1.d0/tau_ave !eq 14 PHYSICAL REVIEW E 95, 033306 (2017)
                    omega_c(i,j) = myomega
                    
                    alpha_ave=0.5d0*(1.d0+phase_field)*alpha_r+0.5d0*(1.d0-phase_field)*alpha_b
		            
#ifdef CGCORR

                      psi_dens(1:2)=0.0d0
					  !
					  do mm=1,8
						psi_dens(1)=psi_dens(1)+ (p(mm)/cssq)*dex(mm)*(myrhoR(mm)+myrhoB(mm))
						psi_dens(2)=psi_dens(2)+ (p(mm)/cssq)*dey(mm)*(myrhoR(mm)+myrhoB(mm))
					  enddo
					  
					  mat_dens=tensor_product(myvel,psi_dens)
					  mat_Tdens=transpose(mat_dens)
					  myG=(mat_dens+mat_Tdens)
					  
					  dot_vel_psi=dot_product(myvel,psi_dens)
					  
!					  corr_c(0)=-3.d0*visc_ave*dot_vel_psi
!					  cmattemp=cmat(1:2,1:2,0)
!					  corr_c(0)=visc_ave*(psi(0)*dot_vel_psi+xi(0)*tensor_contr_tr(myG,cmattemp))
					  do mm=0,8
					    cmattemp=cmat(1:2,1:2,mm)
					    corr_c(mm)=visc_ave*(psi(mm)*dot_vel_psi+xi(mm)*tensor_contr_tr(myG,cmattemp))
					  enddo
	!				  do mm=5,8
!					    cmattemp=cmat(1:2,1:2,mm)
!					    corr_c(mm)=xi(mm)*visc_ave*tensor_contr_tr(myG,cmattemp)
	!				  enddo
#else
                      corr_c(0:8)=0.d0
#endif
                    !comute equilibrium
		            uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq

		            do mm=0,8
		              udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq	
		              ! page eq 3.4 pag 61 kruger	
		              feq(mm)=totrho*(phi(mm) + alpha_ave*varphi(mm) + &
		               p(mm)*( udotc + 0.5d0*udotc*udotc - uu)) &
		               +corr_c(mm) !4.5 of the book
		            enddo
		            
		            do mm=0,8
		              freg(mm)=fpop(mm,1,i,j,flip)+fpop(mm,2,i,j,flip)
                    enddo
#ifdef REGULARIZED
					!!!!start of regularization	
						!i am using f(1)=fneq first part of Eq4 of Mathematics and Computers in Simulation 72 (2006) 165168
				    
				    pxx=0.0d0
		            pyy=0.0d0
		            pxy=0.0d0
		            
                    do mm=0,8
                    ! i am already mixing using first parto of Eq 5 of Mathematics and Computers in Simulation 72 (2006) 165168
                      fneq= freg(mm) - feq(mm)
                      pxx=pxx + (dex(mm)*dex(mm))*fneq
			          pyy=pyy + (dey(mm)*dey(mm))*fneq
			          pxy=pxy +  dex(mm)*dey(mm)*fneq
                    enddo
                    do mm=0,8
                    !eq6 of Mathematics and Computers in Simulation 72 (2006) 165168
                      fneqreg(mm) = ((0.5d0*p(mm))/(cssq**2.d0))*((dex(mm)*dex(mm) - cssq)*pxx + &
		               (dey(mm)*dey(mm) - cssq)*pyy+2.0d0*dex(mm)*dey(mm)*pxy)
                      freg(mm)= feq(mm) + fneqreg(mm)
                    enddo
                    !!!!end of regularization
#endif                
                    
                    
                    
                    uxy_shifted(1)=uxy(1,i,j)+fx_LB/totrho
                    uxy_shifted(2)=uxy(2,i,j)+fy_LB/totrho
                    uu_shifted=0.5d0*(uxy_shifted(1)*uxy_shifted(1) + &
                     uxy_shifted(2)*uxy_shifted(2))/cssq

				    do mm=0,8
                        udotc_shifted=(uxy_shifted(1)*dex(mm) + uxy_shifted(2)*dey(mm))/cssq
                        ! page eq 3.4 pag 61 kruger
                        feq_shifted(mm)=totrho* &
                         (phi(mm) + alpha_ave*varphi(mm)  + &
                          p(mm)*(udotc_shifted + 0.5d0*udotc_shifted*udotc_shifted - uu_shifted)) &
                          + corr_c(mm)
                        ! page eq 3.8 pag 66 kruger + kupershtohk eq 6.32 pag 243 kruger
                        freg(mm)=freg(mm) + myomega*(feq(mm) - freg(mm)) + &
                         (feq_shifted(mm) -feq(mm)) 
                    enddo
					  
                    
                    ckl_RB=min(1.d0,1.0d6*rho(1,i,j)*rho(2,i,j))
                
                    if(ckl_RB>=0.1d0)then
                    
                    
                    !eq. 27 Leclaire et al. Journal of Computational Physics 246 (2013): 318-342.
                    acoeff=(9.0d0/4.0d0)*omega_c(i,j)*surf_tens 
					! if(isnan(acoeff)) then
					

                    psix = 0.0d0
                    psiy = 0.d0
                    do mm=1,8
                      phase_temp = (myrhoR(mm)/mydensR-myrhoB(mm)/mydensB)/ &
					   (myrhoR(mm)/mydensR+myrhoB(mm)/mydensB)
                        psix = psix + (p(mm)/cssq)*dex(mm)*phase_temp
                        psiy = psiy + (p(mm)/cssq)*dey(mm)*phase_temp
                        
                    enddo
                    
                    psinorm_sq = psix**2.d0 + psiy**2.d0
                    psinorm=dsqrt(psinorm_sq)
                    

                    
                    do mm=0,8
                        e_dot_psi(mm)=dex(mm)*psix + dey(mm)*psiy
                        if(psinorm_sq.ne.0.d0)then
                          temp=psinorm*(p(mm)*e_dot_psi(mm)**2.d0/psinorm_sq - b_l(mm))
                        else
                          temp=0.0d0
                        endif
                        !perturbation step on the blind ditros
                        freg(mm)=freg(mm) + acoeff*temp*ckl_RB 
                    enddo
                    !!!!!!!!!!!recoloring step
                    do mm=0,8
                        feq(mm)=totrho*(phi(mm) + alpha_ave*varphi(mm)) 
                    enddo 
                    do mm=0,8
                        temp=dsqrt(dex(mm)**2.d0 + dey(mm)**2.d0)*psinorm
                        if(temp.ne.0.d0)then
                          cosphi=e_dot_psi(mm)/temp
                        else
                          cosphi=0.d0
                        endif
                        !if(isnan(cosphi).or.dabs(cosphi).gt.10.0d0 ) cosphi=0.0d0
						
                        !
                        temp=beta_st*myrhoR(0)*myrhoB(0)*cosphi/totrho**2.d0
                        fpop(mm,1,i,j,flip)=freg(mm)*myrhoR(0)/totrho + temp*feq(mm) !4.17 of book
                        fpop(mm,2,i,j,flip)=freg(mm)*myrhoB(0)/totrho - temp*feq(mm) !4.18
                    enddo
                  else
                    do mm=0,8
                      fpop(mm,1,i,j,flip)=freg(mm)*myrhoR(0)/totrho
                      fpop(mm,2,i,j,flip)=freg(mm)*myrhoB(0)/totrho
                    enddo
                  endif                   
                endif
            enddo
        enddo
        !$omp end do
      else
        ic=1
        !eq 6.16 pag 237 kruger
        !$omp do schedule(static,mychunk) private(j)
        do j=1,ny
            do i=1,nx
                if(isfluid_LB(i,j).eq.1)then
                
                    pxx=0.0d0
		            pyy=0.0d0
		            pxy=0.0d0
                        !comute equilibrium
		            uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
		            myrho=rho(ic,i,j)
		            do mm=0,8
		              udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq	
		              ! page eq 3.4 pag 61 kruger	
		              feq(mm)=p(mm)*myrho*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
		            enddo
#ifdef REGULARIZED 

					!!!!start of regularization	
				    !i am using f(1)=fneq first part of Eq4 of Mathematics and Computers in Simulation 72 (2006) 165168
                    do mm=0,8
                    ! i am already mixing using first parto of Eq 5 of Mathematics and Computers in Simulation 72 (2006) 165168
                      fneq= fpop(mm,ic,i,j,flip) - feq(mm)
                      pxx=pxx + (dex(mm)*dex(mm) - cssq)*fneq
			          pyy=pyy + (dey(mm)*dey(mm) - cssq)*fneq
			          pxy=pxy +  dex(mm)*dey(mm)*fneq
                    enddo
                    do mm=0,8
                    !eq6 of Mathematics and Computers in Simulation 72 (2006) 165168
                      fneqreg(mm) = ((0.5d0*p(mm))/(cssq**2.d0))*((dex(mm)*dex(mm) - cssq)*pxx + &
		               (dey(mm)*dey(mm) - cssq)*pyy+2.0d0*dex(mm)*dey(mm)*pxy)
                      freg(mm)= feq(mm) + fneqreg(mm)
                    enddo
                    !!!!end of regularization


#endif 


                    tau_temp=visc_LB/cssq+0.5d0 !ansat
                    newtau_temp=tau_temp  
                   
                    taufield(i,j)=newtau_temp
                    myomega=1.0d0/newtau_temp 
                    
  
                    uxy_shifted(1)=uxy(1,i,j)+fx_LB/myrho
                    uxy_shifted(2)=uxy(2,i,j)+fy_LB/myrho
                    uu_shifted=0.5d0*(uxy_shifted(1)*uxy_shifted(1) + &
                     uxy_shifted(2)*uxy_shifted(2))/cssq
                    do mm=0,8
                        udotc_shifted=(uxy_shifted(1)*dex(mm) + uxy_shifted(2)*dey(mm))/cssq
                        ! page eq 3.4 pag 61 kruger
                        feq_shifted(mm)=p(mm)*myrho* &
                         (1.0d0 + udotc_shifted + 0.5d0*udotc_shifted*udotc_shifted - uu_shifted)
                        ! page eq 3.8 pag 66 kruger + kupershtohk eq 6.32 pag 243 kruger
#ifdef REGULARIZED 
                        fpop(mm,ic,i,j,flip)=freg(mm) + myomega*(feq(mm) - freg(mm)) + &
                         (feq_shifted(mm) -feq(mm)) 
#else
                        fpop(mm,ic,i,j,flip)= &
                         fpop(mm,ic,i,j,flip)+myomega*(feq(mm)-fpop(mm,ic,i,j,flip))+ &
                         (feq_shifted(mm) -feq(mm)) 
#endif
                    enddo
                endif
            enddo
        enddo
        !$omp end do
      endif
    end subroutine collision_EDM_NW
  
  function aveq(dR,dB,vR,vB,phfield,d,q)
  
  implicit none
  
  real(kind=8), intent(in) :: dR,dB,vR,vB,phfield,d
  integer, intent(in) :: q
  real(8) :: aveq,totd,alpha,beta,kappa,eta,xi,tR,tB
  
  totd=dR+dB
  select case(q)
  case(2)
    tR=vR/cssq+0.5d0
    tB=vB/cssq+0.5d0
    alpha=2.d0*tR*tB/(tR+tB)
    beta=2.d0*(tR-alpha)/d
    kappa=-beta/(2.d0*d)
    eta=2.d0*(alpha-tB)/d
    xi=eta/(2.d0*d)
    if(phfield>d)then
      aveq=tR
    elseif(d>=phfield .and. phfield>0.d0)then
      aveq=alpha+beta*phfield+kappa*phfield**2.d0
    elseif(phfield>=-d .and. phfield<=0.d0)then
      aveq=alpha+eta*phfield+xi*phfield**2.d0
    elseif(phfield<-d)then
      aveq=tB
    endif
    aveq=cssq*(aveq-0.5d0)
  case(1)
    aveq=dR/totd*vR+dB/totd*vB
  case(-1)
    aveq=1.d0/(dR/totd*(1.d0/vR)+dB/totd*(1.d0/vB))
  case(-2) !eq 22 Color-gradient lattice Boltzmann model with nonorthogonal central moments: Hydrodynamic melt-jet breakup simulations
    aveq=1.d0/(0.5d0*(1.d0+phfield)*(1.d0/vR)+0.5d0*(1.d0-phfield)*(1.d0/vB))
  case(0)
    aveq=((vR**dR)*(vB**dB))**(1.d0/totd)
  case default
    write(6,*)'Error in aveq'
    stop
  end select
  
  return
  
  end function aveq
   
  pure function tensor_product(v,w)

  implicit none

  real(kind=8), intent(in),dimension(2) :: v,w
  real(kind=8),dimension(2,2) :: tensor_product
  
  tensor_product(1,1)=v(1)*w(1)
  tensor_product(2,1)=v(2)*w(1)
  
  tensor_product(1,2)=v(1)*w(2)
  tensor_product(2,2)=v(2)*w(2)
  
  return

 end function tensor_product
 
  pure function tensor_contr_tr(v,w)

  implicit none

  real(kind=8), intent(in),dimension(2,2) :: v,w
  real(kind=8) :: tensor_contr_tr
  
  
  !tensor_contr_tr=v(1,1)*wt(1,1)+v(2,2)*wt(2,2)
  tensor_contr_tr=v(1,1)*w(1,1)+v(1,2)*w(1,2)+v(2,1)*w(2,1)+v(2,2)*w(2,2)
  
  return

 end function tensor_contr_tr
 
 pure function tensor_contr_tr2(v,w,myndir,myi)

  implicit none
  
  integer, intent(in) :: myndir,myi
  real(kind=8), intent(in),dimension(2,2) :: v
  real(kind=8), intent(in),dimension(2,2,myndir) :: w
  real(kind=8),dimension(2,2) :: wt
  real(kind=8) :: tensor_contr_tr2
  
  wt(1:2,1:2) = w(1:2,1:2,myi)
  wt = transpose(wt)
  
  tensor_contr_tr2=v(1,1)*wt(1,1)+v(2,2)*wt(2,2)
  
  return

 end function tensor_contr_tr2
    
    !NEWTON METHOD to calculate c1 from (B.38)
    function newtonforncomp2(c0,hlength)
        implicit none
        real*8 :: newtontol,c0,c01,c2,hlength,newtonforncomp2
        integer :: j
        !
        newtontol=1.d01
        c01=c0
        j=0
        do while(newtontol .ge. 1.d-18) !when delta c1 <1.d-18, then newton method stops
           j=j+1
           c2=c01-( ( (1.d0/consistency)**(1.d0/flowindex) )*(flowindex/(flowindex+1.d0))* &
             (( fx_LB*hlength+c01)**(1/flowindex+1.d0)-c01**(1.d0/flowindex+1.d0))/fx_LB +&
             (c01/dynamic_visc_LB)*hlength-(fx_LB/(2.d0*dynamic_visc_LB))*hlength**(2.d0))/ &
             ( (1.d0/dynamic_visc_LB)*hlength+ ((1.d0/consistency)**(1.d0/flowindex))* &
             ( (fx_LB*hlength+c01)**(1.d0/flowindex) -c01**(1.d0/flowindex))/fx_LB)
             !formula by using f(c1): (B. 38) and f'(c1) with dervation of c1
             ! c1-f(c1)/f'(c1)
            newtontol=dabs(c2-c01) !abs(delta c1)
            write(*,'(4x, I2, 4x, f22.18, 4x, ES16.8E3)') j, &
                            c2,newtontol
            c01=c2
        end do
        newtonforncomp2=c01  
        
    end function newtonforncomp2
    
    
    subroutine funcd(x,hlengths,fval,fderiv)


    implicit none
  
    real(kind=8),intent(in) :: x,hlengths
    real(kind=8) :: fval,fderiv,fval2,fderiv2
    !real(kind=8)  :: y, dt

     !fval   = x*sin(x) - 1.0
     !fderiv = x*cos(x) + sin(x)

    fval   = (1.d0/consistency)**(1.d0/flowindex)*(flowindex/(flowindex+1.d0))* &
     ((fx_LB*hlengths+x)**(1.d0/flowindex+1.d0)-x**(1.d0/flowindex+1.d0))/fx_LB+ &
     x/dynamic_visc_LB*hlengths-fx_LB/(2.d0*dynamic_visc_LB)*hlengths**2.d0
    !fval = ( ( (1.d0/consistency)**(1.d0/flowindex) )*(flowindex/(flowindex+1.d0))* &
    !         (( fx_LB*hlengths+x)**(1/flowindex+1.d0)-x**(1.d0/flowindex+1.d0))/fx_LB +&
    !         (x/dynamic_visc_LB)*hlengths-(fx_LB/(2.d0*dynamic_visc_LB))*hlengths**(2.d0))
    fderiv = (1.d0/consistency)**(1.d0/flowindex)* &
     ((fx_LB*hlengths+x)**(1.d0/flowindex) &
     -x**(1.d0/flowindex))/fx_LB +hlengths/dynamic_visc_LB
    !fderiv = ( ((1.d0/consistency)**(1.d0/flowindex))* &
    !         ( (fx_LB*hlengths+x)**(1.d0/flowindex) -x**(1.d0/flowindex))/fx_LB &
    !         + (1.d0/dynamic_visc_LB)*hlengths)

    end subroutine funcd
    
    subroutine cmpfuncd(x,hlengths,fval,fderiv)


    implicit none
  
    real(kind=8),intent(in) :: hlengths
    complex(kind=16),intent(in) :: x
    complex(kind=16) :: fval,fderiv,qconsistency,qflowindex,qfx_LB,&
     qhlengths,qdynamic_visc_LB
    complex(kind=16), parameter :: qone=cmplx(1.d0,0.d0,kind=16)
    
    qconsistency=cmplx(consistency,0.d0,kind=16)
    qflowindex=cmplx(flowindex,0.d0,kind=16)
    qfx_LB=cmplx(fx_LB,0.d0,kind=16)
    qhlengths=cmplx(hlengths,0.d0,kind=16)
    qdynamic_visc_LB=cmplx(dynamic_visc_LB,0.d0,kind=16)
    
    !real(kind=8)  :: y, dt

     !fval   = x*sin(x) - 1.0
     !fderiv = x*cos(x) + sin(x)

    fval   = (qone/qconsistency)**(qone/qflowindex)*(qflowindex/(qflowindex+qone))* &
     ((qfx_LB*qhlengths+x)**(qone/qflowindex+qone)-x**(qone/qflowindex+qone))/qfx_LB+ &
     x/qdynamic_visc_LB*qhlengths-qfx_LB/(2.d0*qdynamic_visc_LB)*qhlengths**2.d0
    
    !fval = ( ( (1.d0/consistency)**(1.d0/flowindex) )*(flowindex/(flowindex+1.d0))* &
    !         (( fx_LB*hlengths+x)**(1/flowindex+1.d0)-x**(1.d0/flowindex+1.d0))/fx_LB +&
    !         (x/dynamic_visc_LB)*hlengths-(fx_LB/(2.d0*dynamic_visc_LB))*hlengths**(2.d0))
    fderiv = (qone/qconsistency)**(qone/qflowindex)* &
     ((qfx_LB*qhlengths+x)**(qone/qflowindex) &
     -x**(qone/qflowindex))/qfx_LB +qhlengths/qdynamic_visc_LB
    !fderiv = ( ((1.d0/consistency)**(1.d0/flowindex))* &
    !         ( (fx_LB*hlengths+x)**(1.d0/flowindex) -x**(1.d0/flowindex))/fx_LB &
    !         + (1.d0/dynamic_visc_LB)*hlengths)

    end subroutine cmpfuncd
    
    subroutine nfuncd(x,hlengths,fval,fderiv)


    implicit none
  
    real(kind=8),intent(in) :: x,hlengths
    real(kind=8) :: fval,fderiv,fval2,fderiv2
    real(kind=16) :: term1,qconsistency,qflowindex,qfx_LB,qhlengths,qx
    real(kind=16),parameter :: qone=real(1.d0,kind=16)
    !real(kind=8)  :: y, dt

     !fval   = x*sin(x) - 1.0
     !fderiv = x*cos(x) + sin(x)
     
    qconsistency=real(consistency,kind=16)
    qflowindex=real(flowindex,kind=16)
    qfx_LB=real(fx_LB,kind=16)
    qhlengths=real(hlengths,kind=16)
    qx=real(x,kind=16)
    !arise to (qflowindex/(qflowindex+qone))
    term1 = (qone/qconsistency)**(qone/(qflowindex+qone))* &
     ((qflowindex/(qflowindex+qone))**(qflowindex/(qflowindex+qone)))* &
     ((qfx_LB*qhlengths+qx)-qx)/(qfx_LB**(qflowindex/(qflowindex+qone)))
    if(dble(term1)<0.d0)then
      write(6,*)' negative term1 in point a',term1
      stop
    endif
    !arise to ((qflowindex+qone)/qflowindex)
    term1=term1**((qflowindex+qone)/qflowindex)
    
    fval   = dble(term1) + &
     x/dynamic_visc_LB*hlengths-fx_LB/(2.d0*dynamic_visc_LB)*hlengths**2.d0
     
    !arise to (qflowindex)
    term1=(qone/qconsistency)* &
     ((qfx_LB*qhlengths+qx)-qx)/(qfx_LB**qflowindex)
    if(dble(term1)<0.d0)then
      write(6,*)' negative term1 in point b',term1
      stop
    endif 
    !arise to (qone/qflowindex)
    term1=term1**(qone/qflowindex)
    
    fderiv = dble(term1) + hlengths/dynamic_visc_LB
    

    end subroutine nfuncd
    
    function rtnewt(x0,TOL,hlengths,lcomplex)

! Function for doing Newton-Raphson iterations



  implicit none
  
  real(kind=8), intent(in)  :: x0, TOL,hlengths
  logical, intent(out) :: lcomplex
  real(kind=8)  :: rtnewt, prev_rtnewt,nrtnewt,prev_nrtnewt
  integer, parameter :: maxit = 1000
  integer       :: j
  real(kind=8) :: fderiv, dx, fval
  complex(kind=16) :: qrtnewt,qfval,qfderiv
  
  lcomplex=.false.
  rtnewt = x0        ! initialising the iterations
     do j = 1, maxit
        prev_rtnewt = rtnewt  ! store previous iterate (x_n)
        
        if(rtnewt<0.d0)then
          write(6,*)'switch to negative eq.'!'ERRORE',rtnewt,fval,fderiv
          lcomplex=.true.
          return
        endif
        call funcd(rtnewt,hlengths, fval, fderiv)
        dx = fval/fderiv   ! fractional term f(x)/df(x)
        rtnewt = rtnewt - dx  ! full iterative step
        write(*,'(4x, i8, 4x, f22.18, 4x, ES16.8E3)') j, &
                            rtnewt, abs(rtnewt - prev_rtnewt)
        if ( abs(rtnewt - prev_rtnewt) < TOL) then
          print*, 'value of j is:',j
          return  
        end if
     end do
     call nrerror('rtnewt exceeded maximum iterations')
     
     return
     
 end function
 
 function qrtnewt(x0,TOL,hlengths)

! Function for doing Newton-Raphson iterations



  implicit none
  
  complex(kind=16), intent(in)  :: x0
  real(kind=8), intent(in)  :: TOL,hlengths
  real(kind=8)  :: rtnewt, prev_rtnewt,nrtnewt
  integer, parameter :: maxit = 1000
  integer       :: j
  
  complex(kind=16) :: qrtnewt,qfval,qfderiv,qprev_rtnewt,qdx
     
     qrtnewt = x0
     do j = 1, maxit
        qprev_rtnewt = qrtnewt  ! store previous iterate (x_n)
        call cmpfuncd(qrtnewt,hlengths,qfval,qfderiv)
        if(isnan(real(qfval)) .or. isnan(real(qfderiv)) .or. &
         isnan(imag(qfval)) .or. isnan(imag(qfderiv)))then
          write(6,*)'ERROR NaN in qrtnewt',qrtnewt,qfval,qfderiv
          stop
        endif
        
        qdx = qfval/qfderiv   ! fractional term f(x)/df(x)
        qrtnewt = qrtnewt - qdx  ! full iterative step
        write(*,'(4x, i8, 4x,f22.18,a,f22.18, 4x,ES16.8E3,a,ES16.8E3)') j, &
                            real(qrtnewt),' +i*',imag(qrtnewt), &
                            abs(real(qrtnewt) - real(qprev_rtnewt)),' +i*', &
                            abs(imag(qrtnewt) - imag(qprev_rtnewt))
        if ( abs(real(qrtnewt) - real(qprev_rtnewt)) < TOL .and. &
         abs(imag(qrtnewt) - imag(qprev_rtnewt)) < TOL ) then
          print*, 'value of j is:',j
          return  
        end if
     end do
     call nrerror('rtnewt exceeded maximum iterations')
     
     return
         
     
 end function qrtnewt
 
 	FUNCTION zbrent(x1,x2,tol,hlengths)
	
	IMPLICIT NONE
	REAL(8), INTENT(IN) :: x1,x2,tol,hlengths
	REAL(8) :: zbrent
    
	INTEGER, PARAMETER :: ITMAX=100
	REAL(8), PARAMETER :: EPS=epsilon(x1)
	INTEGER :: iter
	REAL(8) :: a,b,c,d,e,fa,fb,fc,p,q,r,s,tol1,xm,fderiv
	a=x1
	b=x2
	call funcd(a,hlengths, fa, fderiv)
	call funcd(b,hlengths, fb, fderiv)
	if ((fa > 0.0 .and. fb > 0.0) .or. (fa < 0.0 .and. fb < 0.0)) &
		call nrerror('root must be bracketed for zbrent')
	c=b
	fc=fb
	do iter=1,ITMAX
		if ((fb > 0.0 .and. fc > 0.0) .or. (fb < 0.0 .and. fc < 0.0)) then
			c=a
			fc=fa
			d=b-a
			e=d
		end if
		if (abs(fc) < abs(fb)) then
			a=b
			b=c
			c=a
			fa=fb
			fb=fc
			fc=fa
		end if
		tol1=2.0d0*EPS*abs(b)+0.5d0*tol
		xm=0.5d0*(c-b)
		if (abs(xm) <= tol1 .or. fb == 0.0) then
			zbrent=b
			RETURN
		end if
		if (abs(e) >= tol1 .and. abs(fa) > abs(fb)) then
			s=fb/fa
			if (a == c) then
				p=2.0d0*xm*s
				q=1.0d0-s
			else
				q=fa/fc
				r=fb/fc
				p=s*(2.0d0*xm*q*(q-r)-(b-a)*(r-1.0d0))
				q=(q-1.0d0)*(r-1.0d0)*(s-1.0d0)
			end if
			if (p > 0.0) q=-q
			p=abs(p)
			if (2.0d0*p  <  min(3.0d0*xm*q-abs(tol1*q),abs(e*q))) then
				e=d
				d=p/q
			else
				d=xm
				e=d
			end if
		else
			d=xm
			e=d
		end if
		a=b
		fa=fb
		b=b+merge(d,sign(tol1,xm), abs(d) > tol1 )
		call funcd(b,hlengths, fb, fderiv)
	end do
	call nrerror('zbrent: exceeded maximum iterations')
	zbrent=b
	END FUNCTION zbrent
	
 	SUBROUTINE nrerror(string)
	CHARACTER(LEN=*), INTENT(IN) :: string
	write (*,*) 'nrerror: ',string
	STOP 'program terminated by nrerror'
	END SUBROUTINE nrerror
    
    subroutine surface_tension_2c
    
        implicit none
        integer :: i,j,mm,flip,ii,jj
        real*8 :: acoeff,gradRx,gradRy,gradBx,gradBy,phis,psix,psiy,psinorm,psinorm_sq,ckl_RB,temp,cosphi
        real*8,dimension(0:8) :: rhosum,fdum,feq,e_dot_psi,myrhoR,myrhoB
        !
        if(ncomp==1)return
        flip=mod(step-1,2)+1
        !$omp do private(j,gradRx,gradRy,gradBx,gradBy,ii,jj,myrhoR,myrhoB)
        do j=1,ny
            do i=1,nx
                if(isfluid_LB(i,j).ne.1)cycle
                ! Eq.14 Douillet-Grellier, Thomas, et al. Computational Particle Mechanics 6.4 (2019): 695-720.
                phis=(rho(1,i,j)/mydensR - rho(2,i,j)/mydensB)/(rho(1,i,j)/mydensR + rho(2,i,j)/mydensB)
                
                if(dabs(phis).lt.0.999d0)then
                    
                    do mm=0,8
                        ii=i+ex(mm)
!                        if(ii>nx)ii=ii-nx
!                        if(ii<1)ii=ii+nx
                        jj=j+ey(mm)
					    myrhoR(mm)=rho(1,ii,jj)
					    myrhoB(mm)=rho(2,ii,jj)
		            enddo
                    
                    !eq. 27 Leclaire et al. Journal of Computational Physics 246 (2013): 318-342.
                    acoeff=(9.0d0/2.0d0)*omega_c(i,j)*surf_tens 
					! if(isnan(acoeff)) then
					
					! write(6,*) omega_c(i,j),surf_tens,
					! pause	

					! endif					
                    do mm=0,8
                        rhosum(mm)=  myrhoR(mm) + myrhoB(mm)   
                    enddo
                    !
                    gradRx=0.0d0
                    gradBx=0.0d0
                    !
                    gradRy=0.0d0
                    gradBy=0.0d0
                    do mm=1,8
                        gradRx=gradRx + (p(mm)/cssq)*dex(mm)*(myrhoR(mm)/rhosum(mm))
                        gradBx=gradBx + (p(mm)/cssq)*dex(mm)*(myrhoB(mm)/rhosum(mm))
                        !
                        gradRy=gradRy + (p(mm)/cssq)*dey(mm)*(myrhoR(mm)/rhosum(mm))
                        gradBy=gradBy + (p(mm)/cssq)*dey(mm)*(myrhoB(mm)/rhosum(mm))
                    enddo
                    !Eq. 18  Leclaire et al. Journal of Computational Physics 246 (2013): 318-342.
                    psix= (rho(2,i,j)/rhosum(0))*gradRx - (rho(1,i,j)/rhosum(0))*gradBx !!!!*3
                    psiy= (rho(2,i,j)/rhosum(0))*gradRy - (rho(1,i,j)/rhosum(0))*gradBy
                    !
                    psinorm=dsqrt(psix**2 + psiy**2)
                    psinorm_sq=psinorm**2	
                    !dnx(i,j)=-(psix/psinorm) !uscente dalla goccia
                !
                    !dny(i,j)=-(psiy/psinorm)
                    do mm=0,8
                        fdum(mm)=fpop(mm,1,i,j,flip) + fpop(mm,2,i,j,flip) 
                    enddo
                    !ckl_RB=min(1.d0,1.0d6*rho(1,i,j)*rho(2,i,j))
                    do mm=0,8
                        e_dot_psi(mm)=dex(mm)*psix + dey(mm)*psiy
                        if(psinorm_sq.ne.0.d0)then
                          temp=psinorm*(p(mm)*e_dot_psi(mm)**2/psinorm_sq - b_l(mm))
                        else
                          temp=0.0d0
                        endif
                        !perturbation step on the blind ditros
                        fdum(mm)=fdum(mm) + acoeff*temp!*ckl_RB 
                    enddo
                    !!!!!!!!!!!recoloring step
                    do mm=0,8
                        feq(mm)=rho(1,i,j)*myphi(mm,1) + rho(2,i,j)*myphi(mm,2)!zero velocity and sum over the component
                    enddo 
                    do mm=0,8
                        temp=dsqrt(dex(mm)**2 + dey(mm)**2)*psinorm
                        if(temp.ne.0.d0)then
                          cosphi=e_dot_psi(mm)/temp
                        else
                          cosphi=0.d0
                        endif
                        !if(isnan(cosphi).or.dabs(cosphi).gt.10.0d0 ) cosphi=0.0d0
						
                        !
                        temp=beta_st*rho(1,i,j)*rho(2,i,j)*cosphi/rhosum(0)**2
                        fpop(mm,1,i,j,flip)=fdum(mm)*rho(1,i,j)/rhosum(0) + temp*feq(mm) !4.17 of book
                        fpop(mm,2,i,j,flip)=fdum(mm)*rho(2,i,j)/rhosum(0) - temp*feq(mm) !4.18
                    enddo
                endif
            enddo
        enddo
        !$omp enddo
    end subroutine surface_tension_2c
    
    subroutine surface_tension_cap_stress_2c
    
        implicit none
        integer :: i,j,mm,flip,ii,jj
        real*8 :: acoeff,phis,psix,psiy,psinorm,psinorm_sq,ckl_RB,temp,cosphi
        real*8,dimension(0:8) :: rhosum,rhodiff,fdum,feq,e_dot_psi,myrhoR,myrhoB
        !
        if(ncomp==1)return
        flip=mod(step-1,2)+1
        !$omp do private(j,psix,psiy,ii,jj,myrhoR,myrhoB)
        do j=1,ny
            do i=1,nx
                if(isfluid_LB(i,j).ne.1)cycle
                ! Eq.14 Douillet-Grellier, Thomas, et al. Computational Particle Mechanics 6.4 (2019): 695-720.
                phis=(rho(1,i,j)/mydensR - rho(2,i,j)/mydensB)/(rho(1,i,j)/mydensR + rho(2,i,j)/mydensB)
                if(dabs(phis).lt.0.999d0)then
                    
                    do mm=0,8
                        ii=i+ex(mm)
!                        if(ii>nx)ii=ii-nx
!                        if(ii<1)ii=ii+nx
                        jj=j+ey(mm)
					    myrhoR(mm)=rho(1,ii,jj)
					    myrhoB(mm)=rho(2,ii,jj)
		            enddo
                    
                    acoeff=(9.0d0/4.0d0)*omega_c(i,j)*surf_tens   
                    do mm=0,8
                      rhosum(mm)=  myrhoR(mm)/mydensR + myrhoB(mm)/mydensB 
			   	      rhodiff(mm)= myrhoR(mm)/mydensR - myrhoB(mm)/mydensB 						
                    enddo
                    !
                    psix=0.0d0
					psiy=0.0d0
                    do mm=1,8
                        psix=psix + (p(mm)/cssq)*dex(mm)*(rhodiff(mm)/rhosum(mm))
                        psiy=psiy + (p(mm)/cssq)*dey(mm)*(rhodiff(mm)/rhosum(mm))
                    enddo
                    !
                    psinorm=dsqrt(psix**2 + psiy**2)
                    psinorm_sq=psinorm**2
                    
                  !  dnx(i,j)=-(psix/psinorm) !uscente dalla goccia
                !
                  !  dny(i,j)=-(psiy/psinorm)
                    do mm=0,8
                        fdum(mm)=fpop(mm,1,i,j,flip) + fpop(mm,2,i,j,flip) 
                    enddo
                    !ckl_RB=min(1.d0,1.0d6*rho(1,i,j)*rho(2,i,j))
                    do mm=0,8
                        e_dot_psi(mm)=dex(mm)*psix + dey(mm)*psiy
                        temp=psinorm*(p(mm)*e_dot_psi(mm)**2/psinorm_sq - b_l(mm))
                        if(isnan(temp)) temp=0.0d0
                        !perturbation step on the blind ditros
                        fdum(mm)=fdum(mm) + acoeff*temp!*ckl_RB 
                    enddo
                    do mm=0,8
                        feq(mm)=rho(1,i,j)*myphi(mm,1) + rho(2,i,j)*myphi(mm,2)
                    enddo 
                    do mm=0,8
                        temp=dsqrt(dex(mm)**2 + dey(mm)**2)*psinorm
                        cosphi=e_dot_psi(mm)/temp
                        if(isnan(cosphi)) cosphi=0.0d0
                        !
                        temp=beta_st*rho(1,i,j)*rho(2,i,j)*cosphi/rhosum(0)**2
                        fpop(mm,1,i,j,flip)=fdum(mm)*rho(1,i,j)/rhosum(0) + temp*feq(mm)
                        fpop(mm,2,i,j,flip)=fdum(mm)*rho(2,i,j)/rhosum(0) - temp*feq(mm) 
                    enddo
                endif
            enddo
        enddo
        !$omp enddo
    endsubroutine 
    
    subroutine collision_guo
        implicit none
        integer :: mm,i,j,ic,flip
        real*8,dimension(0:8) :: feq,feq2
        real*8 :: uu,uuf,uxy_corr(2),udotc,ufdotc,myrho
        
        flip=mod(step-1,2)+1
        if(ncomp.ne.1)then
          write(6,*)'ncomp==2 not written for collision_guo'
          stop
        endif
        ic=1
        ! page 
        !$omp do schedule(static,mychunk) private(j)
        do j=1,ny
          do i=1,nx
            if(isfluid_LB(i,j).eq.1)then
              myrho=rho(ic,i,j)
              uxy_corr(1)=uxy(1,i,j)+0.5d0*fx_LB/myrho
              uxy_corr(2)=uxy(2,i,j)+0.5d0*fy_LB/myrho
              uu=0.5d0*(uxy_corr(1)*uxy_corr(1)+uxy_corr(2)*uxy_corr(2))/cssq
              uuf = (uxy_corr(1)*fx_LB + uxy_corr(2)*fy_LB)/cssq
              do mm=0,8
                udotc=(uxy_corr(1)*dex(mm) + uxy_corr(2)*dey(mm))/cssq
		        ufdotc = (fx_LB*dex(mm) + fy_LB*dey(mm))/cssq
		        ! page eq 3.4 pag 61 kruger
                feq(mm)=p(mm)*myrho*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
                !GUO pag 243 kruger
                feq2(mm) = p(mm)*(ufdotc-uuf + udotc*ufdotc)
                ! page eq 3.8 pag 66 kruger + GUO pag 243 kruger
                fpop(mm,ic,i,j,flip)= fpop(mm,ic,i,j,flip) + omega*( feq(mm) - fpop(mm,ic,i,j,flip) ) + &
                 (1.d0-0.5d0*omega)*feq2(mm)
              enddo
            endif
          enddo
        enddo
        !$omp end do nowait
    endsubroutine
    !**********************************************************!
    subroutine streaming_opt
        implicit none
        
        integer:: i,j,l,flip,flop,ic
        
        flip=mod(step-1,2)+1
        flop=3-flip
!
        !$omp do schedule(static,mychunk) private(j) 
        do j=0,ny+1
            do i=0,nx+1
                if(isfluid_LB(i,j).ne.3) then !stream only if not a dead node
                  do ic=1,ncomp
                    do l=0,8 
                        fpop(l,ic,i+ex(l),j+ey(l),flop) = fpop(l,ic,i,j,flip)
                    enddo
                  enddo
                endif
            enddo      
        enddo
        !$omp end do

    endsubroutine streaming_opt
    
    !**********************************************************!
    subroutine all_periodic_bcs
        
        implicit none
        
        integer :: flip
        
        flip=mod(step-1,2)+1
        
        !$omp single
        !west
        fpop(:,:,0,1:ny,flip)=fpop(:,:,nx,1:ny,flip)
        !east
        fpop(:,:,nx+1,1:ny,flip)=fpop(:,:,1,1:ny,flip)
        !north
        fpop(:,:,1:nx,ny+1,flip)=fpop(:,:,1:nx,1,flip)
        ! north
        fpop(:,:,1:nx,0,flip)=fpop(:,:,1:nx,ny,flip)
        !corners
        fpop(:,:,0,0,flip)=fpop(:,:,nx,ny,flip)
        fpop(:,:,nx+1,0,flip)=fpop(:,:,1,ny,flip)
        fpop(:,:,0,ny+1,flip)=fpop(:,:,nx,1,flip)
        fpop(:,:,nx+1,ny+1,flip)=fpop(:,:,1,1,flip)
        !$omp end single
    endsubroutine    
    !**********************************************************!
    subroutine mixed_bcs
        implicit none
        real*8,dimension(0:8) :: fdum
        integer :: i,l,j,ii,jj,flip
        
        flip=mod(step-1,2)+1
        
        !$omp single
        
        !bounce back
        do i=1,nx
            do j=1,ny
                if(isfluid_LB(i,j).eq.0)then
                  do l=0,8
                    ii=i+ex(l)
                    jj=j+ey(l)
                    !pbc along x
                    if(ii<1)ii=ii+nx
                    if(ii>nx)ii=ii-nx
                    if(isfluid_LB(ii,jj).eq.1)then
                      fpop(l,1:ncomp,i,j,flip) = fpop(opp(l),1:ncomp,ii,jj,flip)
                    endif
                  enddo
                endif
            enddo
        enddo
        
        !west pbc
        fpop(:,1:ncomp,0,1:ny,flip)=fpop(:,1:ncomp,nx,1:ny,flip)
        !east pbc
        fpop(:,1:ncomp,nx+1,1:ny,flip)=fpop(:,1:ncomp,1,1:ny,flip)
        
        !$omp end single
    endsubroutine mixed_bcs
    
    subroutine fullway_bb
        implicit none
        real*8,dimension(0:8) :: fdum
        integer :: i,l,j,ii,jj,flip,flop,mm
        
        flip=mod(step-1,2)+1
        flop=3-flip
        !$omp single
        
        !bounce back
        do i=1,nx
            do j=1,ny
                if(isfluid_LB(i,j).eq.0)then
                  do mm=0,8
                    ii=i+ex(mm)
                    jj=j+ey(mm)
                    if(ii>nx .or. ii<1)cycle
                    if(jj>ny .or. jj<1)cycle
                    if(isfluid_LB(ii,jj).eq.1)then
                    fpop(mm,1:ncomp,i,j,flip) = fpop(opp(mm),1:ncomp,ii,jj,flip)
                    endif
                  enddo
                endif
            enddo
        enddo
        
        
        !$omp end single
    endsubroutine fullway_bb
    
    subroutine periodic_bcs
        implicit none
        real*8,dimension(0:8) :: fdum
        integer :: i,l,j,ii,jj,flip,flop,mm
        
        flip=mod(step-1,2)+1
        flop=3-flip
        
        
        !$omp single
        
     if(all(lpbc))then
      !west pbc
      fpop(0:8,1:ncomp,0,1:ny,flip)=fpop(0:8,1:ncomp,nx,1:ny,flip)
      !east pbc
      fpop(0:8,1:ncomp,nx+1,1:ny,flip)=fpop(0:8,1:ncomp,1,1:ny,flip)
      !south pbc
      fpop(0:8,1:ncomp,0:nx+1,0,flip)=fpop(0:8,1:ncomp,0:nx+1,ny,flip)
      !north pbc
      fpop(0:8,1:ncomp,0:nx+1,ny+1,flip)=fpop(0:8,1:ncomp,0:nx+1,1,flip)
    elseif(lpbc(1))then
      !west pbc
      fpop(0:8,1:ncomp,0,1:ny,flip)=fpop(0:8,1:ncomp,nx,1:ny,flip)
      !east pbc
      fpop(0:8,1:ncomp,nx+1,1:ny,flip)=fpop(0:8,1:ncomp,1,1:ny,flip)
    elseif(lpbc(2))then  
      !south pbc
      fpop(0:8,1:ncomp,1:nx+1,0,flip)=fpop(0:8,1:ncomp,0:nx+1,ny,flip)
      !north pbc
      fpop(0:8,1:ncomp,1:nx,ny+1,flip)=fpop(0:8,1:ncomp,1:nx,1,flip)
    endif
    
        
        
        !$omp end single
        
        !$omp barrier
        
        !$omp do schedule(static,mychunk) private(j) 
        do i=0,nx+1
            do j=0,ny+1
                if(isfluid_LB(i,j).eq.0)then
                  do mm=0,8
                    ii=i+ex(mm)
                    jj=j+ey(mm)
                    if(lpbc(1))then
                          !pbc along x
                          if(ii<1)ii=ii+nx
                          if(ii>nx)ii=ii-nx
                        endif
                        if(lpbc(2))then
                          !pbc along x
                          if(jj<1)jj=jj+ny
                          if(jj>ny)jj=jj-ny
                        endif
                        !I check if I am in the grid and not outsude
                        if(lpbc(1))then
                          if(ii<0 .or. ii>nx)then
                            write(6,*)'ERRORE collision_EDM_NW1'
                            stop
                          endif
                        else
                          if(ii<0 .or. ii>nx)cycle
                        endif
                        if(lpbc(2))then
                          if(jj<0 .or. jj>ny)then
                            write(6,*)'ERRORE collision_EDM_NW2'
                            stop
                          endif
                        else
                          if(jj<0 .or. jj>ny)cycle
                        endif
                    if(isfluid_LB(ii,jj).eq.1)then
                      fpop(mm,1:ncomp,i,j,flip) = fpop(opp(mm),1:ncomp,ii,jj,flip)
                    endif
                  enddo
                endif
            enddo
        enddo
        !$omp end do
        
        !$omp barrier
        

        
    endsubroutine periodic_bcs

    
    
    
    
    !**********************************************************!
    subroutine constant_force
    implicit none
    integer :: mm,i,j,flip,ic
    
    flip=mod(step-1,2)+1
    
    !eq 6.16 pag 237 kruger
    !$omp do schedule(static,mychunk) private(j)
    do j=1,ny
        do i=1,nx
            if(isfluid_LB(i,j).eq.1)then
              do ic=1,ncomp
                do mm=0,8
                    fpop(mm,ic,i,j,flip)=fpop(mm,ic,i,j,flip) + (p(mm)/cssq)*(fx_LB*dex(mm) +fy_LB*dey(mm))
                enddo
              enddo
            endif
        enddo
    enddo
    !$omp end do nowait
    endsubroutine
    !**********************************************************!
    subroutine lattice_partitioning
        
        implicit none
    
        integer :: l,lattgr1,lattgr2

        linkgr1=0
        linkgr2=0
        lattgr1=0
        lattgr2=0            
        do l=1,8
            if(ey(l).lt.0.or.(ex(l).le.0.and.ey(l).eq.0)) then
                lattgr1=lattgr1+1
                linkgr1(lattgr1)=l
            else
                lattgr2=lattgr2+1
                linkgr2(lattgr2)=l  
            endif            
        enddo 
    
        continue
    
    endsubroutine lattice_partitioning
    !**********************************************************!
    subroutine LB_sim_data
    
    implicit none
    
    integer :: mybase,mymod,i,j,jhalf,isum,l,ic,ii,jj
    real*8 :: dsum(ncomp),mycenter(2),radius,mydist,dtemp,yy,myi
    
    
    
    mycenter(1)=dble(nx)/2.d0
    mycenter(2)=dble(ny)/2.d0
    radius=dble(ny)/3.d0
    !set density and velocity
    uxy(:,:,:)=0.d0
    rho(:,:,:)=0.d0
    do j=1,ny
      uxy(1,1:nx,j)=analytV(j) !0.0d0
      uxy(2,1:nx,j)=0.d0
    enddo
    
    if(ldrop)uxy(:,:,:)=0.d0
    if(ncomp>1)then
!      rho(1,1:nx,1:ny)=0.d0
!      rho(2,1:nx,1:ny)=mydensB
!      jhalf=floor(dble(ny)*0.5d0)
!      do j=1,jhalf
!        rho(1,1:nx,j)=mydens
!        rho(2,1:nx,j)=0.d0
!      enddo
!      rho(1,1:nx,jhalf+1)=mydens*0.5d0
!      rho(2,1:nx,jhalf+1)=mydensB*0.5d0
      
      !do j=1,ny
        !yy=dble(i-hlength)-0.5d0
        !dtemp=fcut(yy,0.d0-hwidth,0.d0+hwidth)
        !rho(1,1:nx,j)=(1.d0-dtemp)*mydens
        !rho(2,1:nx,j)=dtemp*mydensB
        
      !enddo
      
      ic=0
          do i=1,hlength
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(myi < -hlengthref)then
                rho(1:ncomp,1:nx,ic)=0.d0
            else
              dtemp=fcut(dabs(myi),hhlengthref-width,hhlengthref+width)
              rho(1,1:nx,ic)=mydensR*dtemp+myzero
              rho(2,1:nx,ic)=mydensB*(1.d0-dtemp)+myzero
            endif
          enddo
          do i=hlength+1,ny
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(dabs(myi) > hlengthref)then
              rho(1:ncomp,1:nx,ic)=0.d0
            else
              dtemp=fcut(dabs(myi),hhlengthref-width,hhlengthref+width)
              rho(1,1:nx,ic)=mydensR*dtemp+myzero
              rho(2,1:nx,ic)=mydensB*(1.d0-dtemp)+myzero
            endif
          enddo
        !rho(1,1:nx,1:ny)=myzero
        !rho(2,1:nx,1:ny)=mydensB
        if(ldrop)then
          do j=1,ny
            do i=1,nx
              mydist=dsqrt((dble(j)-mycenter(2))**2.d0+(dble(i)-mycenter(1))**2.d0)
              dtemp=fcut(mydist,16.d0,22.d0)
              rho(1,i,j)=mydensR*dtemp+myzero
              rho(2,i,j)=mydensB*(1.d0-dtemp)+myzero

            enddo
         enddo
     endif
    else
      rho(1,1:nx,1:ny)=mydensR
    endif
    
    if(all(lpbc))then
      !west pbc
      rho(1:2,0,1:ny)=rho(1:2,nx,1:ny)
      !east pbc
      rho(1:2,nx+1,1:ny)=rho(1:2,1,1:ny)
      !south pbc
      rho(1:2,0:nx+1,0)=rho(1:2,0:nx+1,ny)
      !north pbc
      rho(1:2,0:nx+1,ny+1)=rho(1:2,0:nx+1,1)
    elseif(lpbc(1))then
      !west pbc
      rho(1:2,0,1:ny)=rho(1:2,nx,1:ny)
      !east pbc
      rho(1:2,nx+1,1:ny)=rho(1:2,1,1:ny)
    elseif(lpbc(2))then  
      !south pbc
      rho(1:2,1:nx+1,0)=rho(1:2,0:nx+1,ny)
      !north pbc
      rho(1:2,1:nx,ny+1)=rho(1:2,1:nx,1)
    endif
    
    
    mybase=floor(dble(ny)/dble(threads))
    if(mod(ny,mybase).ne.0)then
      mychunk=mybase+1
    else
      mychunk=mybase
    endif
    
    
    !print data
    write(6,*) '*******************LB data*****************'
    write(6,*) 'nx',nx
    write(6,*) 'ny',ny
    write(6,*) 'ncomp',ncomp
    write(6,*) 'surf_tens',surf_tens
    write(6,*) 'beta_st',beta_st
    write(6,*) 'dens_ratio',dens_ratio,(1.d0-alpha_b)/(1.d0-alpha_r)
    write(6,*) 'alpha_r',alpha_r
    write(6,*) 'alpha_b',alpha_b
    write(6,*) 'uxy',uxy(1:2,nx/2,ny/2)
    write(6,*) 'mydensR',mydensR
    write(6,*) 'mydensB',mydensB
    write(6,*) 'tau',tau
    write(6,*) 'visc',visc_LB
    write(6,*) 'fx_LB',fx_LB
    write(6,*) 'fy_LB',fy_LB
#ifdef _OPENMP 
    write(6,*) 'threads',threads
    write(6,*) 'omp chunk',mychunk
#endif
    write(6,*) 'consistency',consistency
    write(6,*) 'flowindex',flowindex
    write(6,*) 'zeroshear',zeroshear
    write(6,*) 'yieldst',yieldst
    write(6,*) 'maxvisc',maxvisc
    write(6,*) 'myerror',myerror
    write(6,*) 'convlimit',convlimit
    write(6,*) 'lfaketwo',lfaketwo
    write(6,*) 'lmatlab',lmatlab
    write(6,*) '*******************************************'
    endsubroutine
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    subroutine wet_wall
    
    implicit none
    
    integer :: mybase,mymod,i,j,jhalf,isum,l,ic,ii,jj
    real*8 :: dsum(ncomp)

    !$omp single
    if(all(lpbc))then
      !west pbc
      rho(1:2,0,1:ny)=rho(1:2,nx,1:ny)
      !east pbc
      rho(1:2,nx+1,1:ny)=rho(1:2,1,1:ny)
      !south pbc
      rho(1:2,0:nx+1,0)=rho(1:2,0:nx+1,ny)
      !north pbc
      rho(1:2,0:nx+1,ny+1)=rho(1:2,0:nx+1,1)
    elseif(lpbc(1))then
      !west pbc
      rho(1:2,0,1:ny)=rho(1:2,nx,1:ny)
      !east pbc
      rho(1:2,nx+1,1:ny)=rho(1:2,1,1:ny)
    elseif(lpbc(2))then  
      !south pbc
      rho(1:2,1:nx+1,0)=rho(1:2,0:nx+1,ny)
      !north pbc
      rho(1:2,1:nx,ny+1)=rho(1:2,1:nx,1)
    endif
    !$omp end single
    
    !$omp barrier
    
    !$omp do schedule(static,mychunk) private(j) 
     do j=1,ny
      do i=1,nx
        if(isfluid_LB(i,j)==0)then !isflui==0 I am at the wall
!          rho(2,i,j)=mydensB
!          rho(1,i,j)=0.d0
!          continue
        
          dsum(1:ncomp)=0.d0
          isum=0
          do l=1,8
            ii=i+ex(l)
            jj=j+ey(l)
            if(lpbc(1))then
              !pbc along x
              if(ii<1)ii=ii+nx
              if(ii>nx)ii=ii-nx
            endif
            if(lpbc(2))then
              !pbc along x
              if(jj<1)jj=jj+ny
              if(jj>ny)jj=jj-ny
            endif
            !I check if I am in the grid and not outsude
            if(ii<0 .or. ii>nx+1)cycle
            if(jj<0 .or. jj>ny+1)cycle
            if(isfluid_LB(ii,jj)==1)then
              isum=isum+1
              do ic=1,ncomp
                dsum(ic)=dsum(ic)+rho(ic,ii,jj)
              enddo
            endif
          enddo
          if(isum==0)then
            write(6,*)'ERROR in iterpolating the density'
            stop
          endif
          rho(1:ncomp,i,j)=dsum(1:ncomp)/dble(isum)
        endif
      enddo
    enddo
    !$omp end do
    
    !$omp barrier
    
    !$omp single
    if(all(lpbc))then
      !west pbc
      rho(1:2,0,1:ny)=rho(1:2,nx,1:ny)
      !east pbc
      rho(1:2,nx+1,1:ny)=rho(1:2,1,1:ny)
      !south pbc
      rho(1:2,0:nx+1,0)=rho(1:2,0:nx+1,ny)
      !north pbc
      rho(1:2,0:nx+1,ny+1)=rho(1:2,0:nx+1,1)
    elseif(lpbc(1))then
      !west pbc
      rho(1:2,0,1:ny)=rho(1:2,nx,1:ny)
      !east pbc
      rho(1:2,nx+1,1:ny)=rho(1:2,1,1:ny)
    elseif(lpbc(2))then  
      !south pbc
      rho(1:2,1:nx+1,0)=rho(1:2,0:nx+1,ny)
      !north pbc
      rho(1:2,1:nx,ny+1)=rho(1:2,1:nx,1)
    endif
    !$omp end single
    
    !$omp barrier
    
   end subroutine
    
    !**********************************************************!
    subroutine init_LB
    
    implicit none
    integer:: i,j,ll,ii,jj,mm,flip,ic
    real*8 :: uu,udotc,acc(ncomp),utemp(2),icc,phase_field,alpha_ave
    
    flip=mod(step-1,2)+1
    
    fpop(:,:,:,:,:)=-666.d0
    !initialize distribution
    if(ncomp==1)then
      ic=1
      do j=1,ny
        do i=1,nx
          if(isfluid_LB(i,j).eq.1)then
            uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
            do mm=0,8
              udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
              fpop(mm,ic,i,j,flip)=p(mm)*rho(ic,i,j)* &
               (1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
            enddo
          endif
        enddo
      enddo
    else
      do j=1,ny
        do i=1,nx
          if(isfluid_LB(i,j).eq.1)then
            phase_field= (rho(1,i,j)/mydensR-rho(2,i,j)/mydensB)/ &
					   (rho(1,i,j)/mydensR+rho(2,i,j)/mydensB)
		    alpha_ave=0.5d0*(1.d0+phase_field)*alpha_r+0.5d0*(1.d0-phase_field)*alpha_b
            uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
            do ic=1,ncomp
              do mm=0,8
                udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
                fpop(mm,ic,i,j,flip)=rho(ic,i,j)*(phi(mm)+alpha_ave*varphi(mm) + &
                 p(mm)*(udotc + 0.5d0*udotc*udotc - uu))
              enddo
            enddo
          endif
        enddo
      enddo
    endif
    
    return
    
    do j=0,ny+1
      do i=0,nx+1
        if(isfluid_LB(i,j).eq.0)then
          acc(1:ncomp)=0.d0
          utemp(1:2)=0.d0
          icc=0.d0
          do ll=1,8
            ii=i+ex(ll)
            jj=j+ey(ll)
            if(ii.gt.0 .and. ii.lt.nx+1 .and. jj.gt.0 .and. jj.lt.ny+1)then
              if(isfluid_LB(ii,jj).eq.1)then
                acc(1:ncomp)=rho(1:ncomp,ii,jj)+acc(1:ncomp)
                utemp(1)=uxy(1,ii,jj)+utemp(1)
                utemp(2)=uxy(2,ii,jj)+utemp(2)
                icc=1.d0+icc               
              endif
            endif
          enddo
          acc(1:ncomp)=acc(1:ncomp)/icc
          utemp(1:2)=utemp(1:2)/icc
          uu=0.5d0*(utemp(1)*utemp(1) + utemp(2)*utemp(2))/cssq
            do ic=1,ncomp
              do mm=0,8
                udotc=(utemp(1)*dex(mm) + utemp(2)*dey(mm))/cssq
                fpop(mm,ic,i,j,flip)=acc(ic)*(myphi(mm,ic)+ &
                 p(mm)*(udotc + 0.5d0*udotc*udotc - uu))
              enddo
            enddo
        endif
      enddo
    enddo
    
    endsubroutine
    !**********************************************************!
    subroutine geometry 
        implicit none
        integer :: i,j,ll,ii,jj
        ! initialize geometry   1=fluid node     0=bounce back      3=dead node
        isfluid_LB=3
       ! isfluid_LB(1:nx,2:ny-1)=1
        if(all(lpbc))then
          isfluid_LB(1:nx,1:ny)=1
        elseif(lpbc(1))then
          isfluid_LB(1:nx,2:ny-1)=1
        elseif(lpbc(2))then
          isfluid_LB(2:nx-1,1:ny)=1
        else
          isfluid_LB(2:nx-1,2:ny-1)=1
        endif
        do i=1,nx
            do j=1,ny
                !dead node with around a fluid node should be set equal to 0
                if(isfluid_LB(i,j).eq.3)then
                    do ll=1,8
                        ii=i+ex(ll)
                        jj=j+ey(ll)
                        if(ii.gt.0 .and. ii.lt.nx+1 .and. jj.gt.0 .and. jj.lt.ny+1)then
                            if(isfluid_LB(ii,jj).eq.1)then
                                isfluid_LB(i,j)=0
                            endif
                        endif
                    enddo
                endif
            enddo
        enddo
        
        if(all(lpbc))then
          !west pbc
          isfluid_LB(0,1:ny)=isfluid_LB(nx,1:ny)
          !east pbc
          isfluid_LB(nx+1,1:ny)=isfluid_LB(1,1:ny)
          !south pbc
          isfluid_LB(0:nx+1,0)=isfluid_LB(0:nx+1,ny)
          !north pbc
          isfluid_LB(0:nx+1,ny+1)=isfluid_LB(0:nx+1,1)
        elseif(lpbc(1))then
          !west pbc
          isfluid_LB(0,1:ny)=isfluid_LB(nx,1:ny)
          !east pbc
          isfluid_LB(nx+1,1:ny)=isfluid_LB(1,1:ny)
        elseif(lpbc(2))then  
          !south pbc
          isfluid_LB(1:nx+1,0)=isfluid_LB(0:nx+1,ny)
          !north pbc
          isfluid_LB(1:nx,ny+1)=isfluid_LB(1:nx,1)
        endif
        
        
!        open(unit=7,file='isfluid.xyz',status='replace')
!        write(7,*)(nx+2)*(ny+2)
!        write(7,*)
!        do j=0,ny+1
!          do i=0,nx+1
!            write(7,*)isfluid_LB(i,j),dble(i),dble(j),1.d0
!          enddo
!        enddo
!        close(7)
!        stop
        
    endsubroutine
    
    subroutine geometry_allpbc
        implicit none
        integer :: i,j,ll,ii,jj
        ! initialize geometry   1=fluid node     0=bounce back      3=dead node
        isfluid_LB=3
        isfluid_LB(1:nx,1:ny)=1

        
        !west pbc
        isfluid_LB(0,1:ny)=isfluid_LB(nx,1:ny)
        !east pbc
        isfluid_LB(nx+1,1:ny)=isfluid_LB(1,1:ny)
        !south pbc
        isfluid_LB(1:nx,0)=isfluid_LB(1:nx,ny)
        !north pbc
        isfluid_LB(1:nx,ny+1)=isfluid_LB(1:nx,1)
        
        isfluid_LB(0,0)=isfluid_LB(nx,ny)
        isfluid_LB(nx+1,ny+1)=isfluid_LB(1,1)
        
        isfluid_LB(nx+1,0)=isfluid_LB(1,ny)
        isfluid_LB(0,ny+1)=isfluid_LB(nx,1)
        
    endsubroutine
    
    subroutine geometry_obs
        implicit none
        integer :: i,j,ll,ii,jj
        real*8 :: icx,icy,myrad,mydist
        ! initialize geometry   1=fluid node     0=bounce back      3=dead node
        isfluid_LB=3
        isfluid_LB(1:nx,2:ny-1)=1
        
        icx=dble(nx)/4.d0
        icy=dble(ny)/2.d0
        myrad=dble(ny)/8.d0
        
        do i=1,nx
          do j=1,ny
            mydist=dsqrt((dble(i)-icx)**2.d0+(dble(j)-icy)**2.d0)
            if(mydist<=myrad)isfluid_LB(i,j)=3
          enddo
        enddo
        
        do i=1,nx
            do j=1,ny
                !dead node with around a fluid node should be set equal to 0
                if(isfluid_LB(i,j).eq.3)then
                    do ll=1,8
                        ii=i+ex(ll)
                        jj=j+ey(ll)
                        if(ii.gt.0 .and. ii.lt.nx+1 .and. jj.gt.0 .and. jj.lt.ny+1)then
                            if(isfluid_LB(ii,jj).eq.1)then
                                isfluid_LB(i,j)=0
                            endif
                        endif
                    enddo
                endif
            enddo
        enddo
        
        !west pbc
        isfluid_LB(0,1:ny)=isfluid_LB(nx,1:ny)
        !east pbc
        isfluid_LB(nx+1,1:ny)=isfluid_LB(1,1:ny)
        
    endsubroutine
    !**********************************************************!
    subroutine outLB
    
        implicit none
        
        integer :: ii,jj
        ! open wrt files
        !$omp single
        if(mod(step,stamp).eq.0)then
            open(unit=300,file='u'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=301,file='v'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=302,file='rho'//write_fmtnumb(step/stamp)//'.out',status='replace')
            if(ncomp>1)open(unit=303,file='rhob'//write_fmtnumb(step/stamp)//'.out',status='replace')
            do jj=1,ny
                do ii=1,nx
                    write(300,*) uxy(1,ii,jj)
                    write(301,*) uxy(2,ii,jj)
                    write(302,*) rho(1,ii,jj)
                    if(ncomp>1)write(303,*) rho(2,ii,jj)
                enddo    
            enddo
            close(300)
            close(301)
            close(302)
            if(ncomp>1)close(303)
            write(6,*) 'printing LB at step', step,uxy(1,45,36)
        endif
        !$omp end single
    endsubroutine
    
  subroutine write_rho_VTI(myframe)
  implicit none
  
  integer, intent(in) :: myframe
  real(4),allocatable, save :: rho4(:,:,:)
  logical, parameter :: textual=.false.
  character(len=120) :: fnameFull,extent
  integer i,j,k, iotest,iotest2
  integer(4)         :: length
  logical,save :: lfirst=.true.
  
  character(len=*), parameter :: fname='dens'
  character(len=*), parameter :: fnameB='densB'
  
  
  if(lfirst)then
    lfirst=.false.
    allocate(rho4(nx,ny,1))
  endif
  
  
  
  rho4(1:nx,1:ny,1)=real(rho(1,1:nx,1:ny),kind=4)

  iotest = 55
  fnameFull = trim(fname) // '_' // trim(write_fmtnumb(myframe)) // '.vti'
  open(unit=iotest,file=trim(fnameFull),status='replace',action='write')

  extent =  trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(nx)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(ny)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(1))

  write(iotest,*) '<VTKFile type="ImageData" version="1.0" byte_order="LittleEndian" >'
  write(iotest,*) ' <ImageData WholeExtent="' // trim(extent) // '" >'
  write(iotest,*) ' <Piece Extent="' // trim(extent) // '">'
  write(iotest,*) '   <PointData>'

  if (textual) then
    write(iotest,*) '    <DataArray type="Float32" Name="',trim(fname),'" format="ascii" >'

    do k=1,1
      do j=1,ny
        do i=1,nx
          write(iotest,fmt='("     ", F20.8)') rho4(i,j,k)
        enddo
      enddo
    enddo

    write(iotest,*) '    </DataArray>'
    write(iotest,*) '   </PointData>'
    write(iotest,*) ' </Piece>'
    write(iotest,*) ' </ImageData>'
  else
    write(iotest,*) '    <DataArray type="Float32" Name="',trim(fname),'" format="appended" offset="0" />'
    write(iotest,*) '   </PointData>'
    write(iotest,*) ' </Piece>'
    write(iotest,*) ' </ImageData>'
    write(iotest,*) ' <AppendedData encoding="raw">'
    close(iotest)

    open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                access='stream',form='unformatted',action='write')
    write(iotest) '_'
    length = 4*nx*ny*1
    write(iotest) length, rho4(1:nx,1:ny,1:1)
    close(iotest)

    open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                action='write')
    write(iotest,*) ''
    write(iotest,*) ' </AppendedData>'
  endif

  write(iotest,*) '</VTKFile >'
  close(iotest)
  
  if(ncomp>1)then
  
  
  rho4(1:nx,1:ny,1)=real(rho(2,1:nx,1:ny),kind=4)

  iotest2 = 56
  fnameFull = trim(fnameB) // '_' // trim(write_fmtnumb(myframe)) // '.vti'
  open(unit=iotest2,file=trim(fnameFull),status='replace',action='write')

  extent =  trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(nx)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(ny)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(1))

  write(iotest2,*) '<VTKFile type="ImageData" version="1.0" byte_order="LittleEndian" >'
  write(iotest2,*) ' <ImageData WholeExtent="' // trim(extent) // '" >'
  write(iotest2,*) ' <Piece Extent="' // trim(extent) // '">'
  write(iotest2,*) '   <PointData>'

  if (textual) then
    write(iotest2,*) '    <DataArray type="Float32" Name="',trim(fnameB),'" format="ascii" >'

    do k=1,1
      do j=1,ny
        do i=1,nx
          write(iotest2,fmt='("     ", F20.8)') rho4(i,j,k)
        enddo
      enddo
    enddo

    write(iotest2,*) '    </DataArray>'
    write(iotest2,*) '   </PointData>'
    write(iotest2,*) ' </Piece>'
    write(iotest2,*) ' </ImageData>'
  else
    write(iotest2,*) '    <DataArray type="Float32" Name="',trim(fnameB),'" format="appended" offset="0" />'
    write(iotest2,*) '   </PointData>'
    write(iotest2,*) ' </Piece>'
    write(iotest2,*) ' </ImageData>'
    write(iotest2,*) ' <AppendedData encoding="raw">'
    close(iotest2)

    open(unit=iotest2,file=trim(fnameFull),status='old',position='append', &
                access='stream',form='unformatted',action='write')
    write(iotest2) '_'
    length = 4*nx*ny*1
    write(iotest2) length, rho4(1:nx,1:ny,1:1)
    close(iotest2)

    open(unit=iotest2,file=trim(fnameFull),status='old',position='append', &
                action='write')
    write(iotest2,*) ''
    write(iotest2,*) ' </AppendedData>'
  endif

  write(iotest2,*) '</VTKFile >'
  close(iotest2)
  
  endif
  
 end subroutine write_rho_VTI
 
 subroutine write_vel_VTI(myframe)
  implicit none
  
  integer, intent(in) :: myframe
  real(4),allocatable, save :: vel4(:,:,:,:)
  logical, parameter :: textual=.false.
  character(len=120) :: fnameFull,extent
  integer i,j,k, iotest
  integer(4)         :: length
  logical,save :: lfirst=.true.
  
  character(len=*), parameter :: fname='vel'
  character(len=*), parameter :: fnameR='huangR'
  character(len=*), parameter :: fnameB='huangB'
  
  if(lfirst)then
    lfirst=.false.
    allocate(vel4(3,nx,ny,1))
    vel4(1:3,1:nx,1:ny,1)=0.0
  endif
  
  vel4(1:2,1:nx,1:ny,1)=real(uxy(1:2,1:nx,1:ny),kind=4)

  iotest = 56
  fnameFull = trim(fname) // '_' // trim(write_fmtnumb(myframe)) // '.vti'
  open(unit=iotest,file=trim(fnameFull),status='replace',action='write')

  extent =  trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(nx)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(ny)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(1))

  write(iotest,*) '<VTKFile type="ImageData" version="1.0">'
  write(iotest,*) ' <ImageData WholeExtent="' // trim(extent) // '" >'
  write(iotest,*) ' <Piece Extent="' // trim(extent) // '">'
  write(iotest,*) '   <PointData>'

  if (textual) then
    write(iotest,*) '    <DataArray type="Float32" Name="vel" NumberOfComponents="3" format="ascii" >'

    do k=1,1
      do j=1,ny
        do i=1,nx
          write(iotest,fmt='("     ", F20.8)') vel4(1,i,j,k),vel4(2,i,j,k),vel4(3,i,j,k)
        enddo
      enddo
    enddo

    write(iotest,*) '    </DataArray>'
    write(iotest,*) '   </PointData>'
    write(iotest,*) ' </Piece>'
    write(iotest,*) ' </ImageData>'
  else
    write(iotest,*) '    <DataArray type="Float32" Name="vel" NumberOfComponents="3" format="appended" offset="0" />'
    write(iotest,*) '   </PointData>'
    write(iotest,*) ' </Piece>'
    write(iotest,*) ' </ImageData>'
    write(iotest,*) ' <AppendedData encoding="raw">'
    close(iotest)

    open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                access='stream',form='unformatted',action='write')
    write(iotest) '_'
    length = 4*nx*ny*1*3
    write(iotest) length, vel4(1:3, 1:nx,1:ny,1:1)
    close(iotest)

    open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                action='write')
    write(iotest,*) ''
    write(iotest,*) ' </AppendedData>'
  endif

  write(iotest,*) '</VTKFile >'
  close(iotest)
  
#ifdef HUANG
  
  
  vel4(1:2,1:nx,1:ny,1)=real(huangR(1:2,1:nx,1:ny),kind=4)

  iotest = 56
  fnameFull = trim(fnameR) // '_' // trim(write_fmtnumb(myframe)) // '.vti'
  open(unit=iotest,file=trim(fnameFull),status='replace',action='write')

  extent =  trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(nx)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(ny)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(1))

  write(iotest,*) '<VTKFile type="ImageData" version="1.0">'
  write(iotest,*) ' <ImageData WholeExtent="' // trim(extent) // '" >'
  write(iotest,*) ' <Piece Extent="' // trim(extent) // '">'
  write(iotest,*) '   <PointData>'
  
  write(iotest,*) '    <DataArray type="Float32" Name="huangR" NumberOfComponents="3" format="appended" offset="0" />'
  write(iotest,*) '   </PointData>'
  write(iotest,*) ' </Piece>'
  write(iotest,*) ' </ImageData>'
  write(iotest,*) ' <AppendedData encoding="raw">'
  close(iotest)

  open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                access='stream',form='unformatted',action='write')
  write(iotest) '_'
  length = 4*nx*ny*1*3
  write(iotest) length, vel4(1:3, 1:nx,1:ny,1:1)
  close(iotest)

  open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                action='write')
  write(iotest,*) ''
  write(iotest,*) ' </AppendedData>'
  write(iotest,*) '</VTKFile >'
  close(iotest)
  
  vel4(1:2,1:nx,1:ny,1)=real(huangB(1:2,1:nx,1:ny),kind=4)

  iotest = 56
  fnameFull = trim(fnameB) // '_' // trim(write_fmtnumb(myframe)) // '.vti'
  open(unit=iotest,file=trim(fnameFull),status='replace',action='write')

  extent =  trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(nx)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(ny)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(1))

  write(iotest,*) '<VTKFile type="ImageData" version="1.0">'
  write(iotest,*) ' <ImageData WholeExtent="' // trim(extent) // '" >'
  write(iotest,*) ' <Piece Extent="' // trim(extent) // '">'
  write(iotest,*) '   <PointData>'
  
  write(iotest,*) '    <DataArray type="Float32" Name="huangB" NumberOfComponents="3" format="appended" offset="0" />'
  write(iotest,*) '   </PointData>'
  write(iotest,*) ' </Piece>'
  write(iotest,*) ' </ImageData>'
  write(iotest,*) ' <AppendedData encoding="raw">'
  close(iotest)

  open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                access='stream',form='unformatted',action='write')
  write(iotest) '_'
  length = 4*nx*ny*1*3
  write(iotest) length, vel4(1:3, 1:nx,1:ny,1:1)
  close(iotest)

  open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                action='write')
  write(iotest,*) ''
  write(iotest,*) ' </AppendedData>'
  write(iotest,*) '</VTKFile >'
  close(iotest)

#endif  
  
 end subroutine write_vel_VTI

    
     !****************************************************************!
     subroutine alloc_LB
        
     implicit none
        real*8 :: v1(2),v2(2)
        integer :: i
        
        allocate(p(0:8),ex(0:8),ey(0:8),dex(0:8),dey(0:8),opp(0:8),b_l(0:8))
        allocate(fpop(0:8,1:ncomp,-1:nx+2,-1:ny+2,2),uxy(2,0:nx+1,0:ny+1), &
         rho(1:ncomp,0:nx+1,0:ny+1))
        allocate(isfluid_LB(0:nx+1,0:ny+1))
        allocate(taufield(1:nx,1:ny))
        allocate(omega_c(1:nx,1:ny))
#ifdef HUANG
        allocate(gradrhoR(2,1:nx,1:ny),gradrhoB(2,1:nx,1:ny))
        allocate(gradrhovR(2,1:nx,1:ny),gradrhovB(2,1:nx,1:ny))
        allocate(huangR(2,1:nx,1:ny),huangB(2,1:nx,1:ny))
        huangR(2,1:nx,1:ny)=0.d0
        huangB(2,1:nx,1:ny)=0.d0
#endif
        fpop(0:8,1:ncomp,-1:nx+2,-1:ny+2,2)=0.d0
		ex=(/0, 1, 0,-1, 0, 1,-1,-1, 1/)
        ey=(/0, 0, 1, 0,-1, 1, 1,-1,-1/)
        dex=dfloat(ex)
        dey=dfloat(ey)
        b_l=(/-4.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0, &
         2.0d0/27.0d0,5.0d0/108.0d0,5.0d0/108.0d0,5.0d0/108.0d0,5.0d0/108.0d0/)
        p=(/4.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0, &
         1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0/)
        opp=(/0,3,4,1,2,7,8,5,6/)
        allocate(regmat(0:8,5))
        regmat(0:8,1)=(/0.d0,-3.d0,0.d0,3.d0,0.d0,-3.d0,3.d0,3.d0,-3.d0/)
        regmat(0:8,2)=(/0.d0,0.d0,-3.d0,0.d0,3.d0,-3.d0,-3.d0,3.d0,3.d0/)
        regmat(0:8,3)=(/-12.d0,6.d0,-12.d0,6.d0,-12.d0,6.d0,6.d0,6.d0,6.d0/)
        regmat(0:8,4)=(/0.d0,0.d0,0.d0,0.d0,0.d0,9.d0,-9.d0,9.d0,-9.d0/)
        regmat(0:8,5)=(/-12.d0,-12.d0,6.d0,-12.d0,6.d0,6.d0,6.d0,6.d0,6.d0/)
        isfluid_LB=3
        rho=0.d0
        uxy=0.d0
        taufield=tau
        allocate(myphi(0:8,ncomp))
        allocate(phi(0:8),varphi(0:8),psi(0:8),xi(0:8))
        if(ncomp==1)then
          myphi(0:8,1)=p(0:8)
        else
          !eq 9 Leclaire et al. Journal of Computational Physics 246 (2013): 318-342.
          !first component
          myphi(0,1)=alpha_r
          myphi(1:4,1)=(1.d0-alpha_r)/5.d0
          myphi(5:8,1)=(1.d0-alpha_r)/20.d0
          !second component
          myphi(0,2)=alpha_b
          myphi(1:4,2)=(1.d0-alpha_b)/5.d0
          myphi(5:8,2)=(1.d0-alpha_b)/20.d0
        endif
        
        phi(0)=0.d0
        phi(1:4)=1.d0/5.d0
        phi(5:8)=1.d0/20.d0
        varphi(0)=1.d0
        varphi(1:4)=-1.d0/5.d0
        varphi(5:8)=-1.d0/20.d0
#if 1        
        psi(0)=-3.d0
        psi(1:4)=0.d0
        psi(5:8)=0.d0
#else        
        psi(0)=-8.d0/3.d0
        psi(1:4)=-1.d0/6.d0
        psi(5:8)=1.d0/12.d0
#endif        
        xi(0)=0.d0
        xi(1:4)=1.d0/2.d0
        xi(5:8)=1.d0/8.d0
        
        allocate(cmat(2,2,0:ndir))
		do i=0,ndir
		  v1(1)=dex(i)
		  v1(2)=dey(i)
	   	  v2(1:2)=v1(1:2)
		  cmat(1:2,1:2,i)=tensor_product(v1,v2)
        enddo
        
     endsubroutine
     !****************************************************************!
     
   subroutine init_random_seed(myseed)
 
	!***********************************************************************
	!     
	!     LBsoft subroutine for initialising the random generator
	!     by the seed given in input file or by a random seed
	!     originally written in JETSPIN by M. Lauricella et al.
	!     
	!     licensed under the 3-Clause BSD License (BSD-3-Clause)
	!     author: M. Lauricella
	!     last modification July 2018
	!     
	!***********************************************************************
	  
	  implicit none
	  
	  integer,intent(in),optional :: myseed
	  integer :: i, n, clock
	  
	  integer, allocatable :: seed(:)
			  
	  call random_seed(size = n)
	  
	  allocate(seed(n))
	  
	  if(present(myseed))then
	!   If the seed is given in input
		seed = myseed*(idrank+1) + 37 * (/ (i - 1, i = 1, n) /)
		
	  else
	!   If the seed is not given in input it is generated by the clock
		call system_clock(count=clock)
			 
		seed = clock*(idrank+1) + 37 * (/ (i - 1, i = 1, n) /)
		
	  endif
	  
	  call random_seed(put = seed)
		   
	  deallocate(seed)
	  
	  return
	 
	 end subroutine init_random_seed
	 
	 pure function rand_noseeded(i,j,k,l)

!***********************************************************************
!     
!     LBsoft random number generator based on the universal
!     random number generator of marsaglia et Al.
!     it is delivered in a form which is depending on a seed squence 
!     provided at each call in order to ensure the MPI independence
!     
!     licensed under the 3-Clause BSD License (BSD-3-Clause)
!     author: M. Lauricella
!     last modification July 2018
!     
!***********************************************************************

  implicit none
      
  integer, intent(in) :: i,j,k,l
  integer :: isub,jsub,ksub,lsub,msub
  integer ::ii,jj
  real(4) :: s,t,u33,u97,csub,uni
  real(kind=8) :: rand_noseeded
  
  real(4), parameter :: c =  362436.0/16777216.0
  real(4), parameter :: cd= 7654321.0/16777216.0
  real(4), parameter :: cm=16777213.0/16777216.0
  
! initial values of i,j,k must be in range 1 to 178 (not all 1)
! initial value of l must be in range 0 to 168.
      
  isub=mod(i,178)
  isub=isub+1
  
  jsub=mod(j,178)
  jsub=jsub+1
  
  ksub=mod(k,178)
  ksub=ksub+1
  
  lsub=mod(l,169)
  
! initialization on fly
  
  ii=97
  s=0.0
  t=0.5
  do jj=1,24
    msub=mod(mod(isub*jsub,179)*ksub,179)
    isub=jsub
    jsub=ksub
    ksub=msub
    lsub=mod(53*lsub+1,169)
    if(mod(lsub*msub,64).ge.32)s=s+t
    t=0.5*t
  enddo
  u97=s
  
  ii=33
  s=0.0
  t=0.5
  do jj=1,24
    msub=mod(mod(isub*jsub,179)*ksub,179)
    isub=jsub
    jsub=ksub
    ksub=msub
    lsub=mod(53*lsub+1,169)
    if(mod(lsub*msub,64).ge.32)s=s+t
    t=0.5*t
  enddo
  u33=s
  uni=u97-u33
  if(uni.lt.0.0)uni=uni+1.0
  csub=c-cd
  if(csub.lt.0.0)csub=csub+cm
  uni=uni-csub
  if(uni.lt.0.0)uni=uni+1.0
  rand_noseeded=real(uni,kind=8)

  return
  
 end function rand_noseeded
 
 function gauss()
 
!***********************************************************************
!     
!     LBsoft subroutine for generating random number normally
!     distributed by the Box-Muller transformation
!     originally written in JETSPIN by M. Lauricella et al.
!     
!     licensed under the 3-Clause BSD License (BSD-3-Clause)
!     author: M. Lauricella
!     last modification January 2020
!     
!***********************************************************************
  
  implicit none
  
  real(kind=8) :: gauss
  real(kind=8) :: dtemp1,dtemp2
  

  real(kind=8), parameter :: mylimit=1.d-100

  
  call random_number(dtemp1)
  call random_number(dtemp2)
  
  dtemp1=dtemp1*(1.d0-mylimit)+mylimit

! Box-Muller transformation
  gauss=sqrt(- 2.d0 *log(dtemp1))*cos(2.d0*pi_greek*dtemp2)
  
  return
  
 end function gauss
 
 pure function gauss_noseeded(i,j,k,l)
 
!***********************************************************************
!     
!     LBsoft subroutine for generating random number normally
!     distributed by the Box-Muller transformation without a seed
!     
!     licensed under the 3-Clause BSD License (BSD-3-Clause)
!     author: M. Lauricella
!     last modification January 2020
!     
!***********************************************************************
  
  implicit none
  integer, intent(in) :: i,j,k,l
  integer :: kk
  real(kind=8) :: gauss_noseeded
  real(kind=8) :: dtemp1,dtemp2

  real(kind=8), parameter :: mylimit=1.d-100

  
  dtemp1=rand_noseeded(i,j,k,l)
  kk=nint(dtemp1*50.d0)
  dtemp2=rand_noseeded(i,j,k,l+kk)
  
  dtemp1=dtemp1*(1.d0-mylimit)+mylimit
  
! Box-Muller transformation
  gauss_noseeded=sqrt(- 2.d0 *log(dtemp1))*cos(2.d0*pi_greek*dtemp2)
  
  return
  
 end function gauss_noseeded  
 
 pure function fcut(r,inner_cut,outer_cut)



  implicit none

  real(kind=8), intent(in) :: r,inner_cut,outer_cut
  real(kind=8) :: fcut
  
  real(kind=8),parameter :: Pi=3.141592653589793238462643383279502884d0

  if ( r <= inner_cut ) then
    fcut = 1.d0
  elseif ( r > outer_cut ) then
      fcut = 0.d0
  else
      fcut = 0.5d0*cos((r-inner_cut)*Pi/(outer_cut-inner_cut))+0.5d0
  endif

  return

 end function fcut 
 
 subroutine write_isfluid_VTI(myframe)
  implicit none
  
  integer, intent(in) :: myframe
  real(4),allocatable, save :: rho4(:,:,:)
  logical, parameter :: textual=.false.
  character(len=120) :: fnameFull,extent
  integer i,j,k, iotest,iotest2
  integer(4)         :: length
  logical,save :: lfirst=.true.
  integer, parameter :: nz=1
  
  character(len=*), parameter :: fname='genisf'
  
  
  
  if(lfirst)then
    lfirst=.false.
    allocate(rho4(nx,ny,nz))
  endif
  
  
  
  rho4(1:nx,1:ny,1)=real(isfluid_LB(1:nx,1:ny),kind=4)

  iotest = 55
  fnameFull = trim(fname) // '_' // trim(write_fmtnumb(myframe)) // '.vti'
  open(unit=iotest,file=trim(fnameFull),status='replace',action='write')

  extent =  trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(nx)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(ny)) // ' ' &
        // trim(write_fmtnumb(1)) // ' ' // trim(write_fmtnumb(nz))

  write(iotest,*) '<VTKFile type="ImageData" version="1.0" byte_order="LittleEndian" >'
  write(iotest,*) ' <ImageData WholeExtent="' // trim(extent) // '" >'
  write(iotest,*) ' <Piece Extent="' // trim(extent) // '">'
  write(iotest,*) '   <PointData>'

  if (textual) then
    write(iotest,*) '    <DataArray type="Float32" Name="',trim(fname),'" format="ascii" >'

    do k=1,nz
      do j=1,ny
        do i=1,nx
          write(iotest,fmt='("     ", F20.8)') rho4(i,j,k)
        enddo
      enddo
    enddo

    write(iotest,*) '    </DataArray>'
    write(iotest,*) '   </PointData>'
    write(iotest,*) ' </Piece>'
    write(iotest,*) ' </ImageData>'
  else
    write(iotest,*) '    <DataArray type="Float32" Name="',trim(fname),'" format="appended" offset="0" />'
    write(iotest,*) '   </PointData>'
    write(iotest,*) ' </Piece>'
    write(iotest,*) ' </ImageData>'
    write(iotest,*) ' <AppendedData encoding="raw">'
    close(iotest)

    open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                access='stream',form='unformatted',action='write')
    write(iotest) '_'
    length = 4*nx*ny*nz
    write(iotest) length, rho4(1:nx,1:ny,1:nz)
    close(iotest)

    open(unit=iotest,file=trim(fnameFull),status='old',position='append', &
                action='write')
    write(iotest,*) ''
    write(iotest,*) ' </AppendedData>'
  endif

  write(iotest,*) '</VTKFile >'
  close(iotest)
  
  
  
 end subroutine write_isfluid_VTI
     
    endmodule
    
