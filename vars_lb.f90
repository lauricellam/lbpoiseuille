    module vars_lb
    
    implicit none 
    integer, parameter :: ndir=8
    real*8, allocatable,dimension(:):: p,dex,dey,b_l,analytV
    integer,allocatable,dimension(:) :: ex,ey,opp,linkgr1,linkgr2
    real*8, allocatable,dimension(:,:,:) :: uxy,rho,gradrhoR,gradrhoB, &
     gradrhovR,gradrhovB,huangR,huangB
    real*8, allocatable,dimension(:,:,:,:,:) :: fpop
    real*8, allocatable,dimension(:) :: phi,varphi,psi,xi
    real*8, allocatable,dimension(:,:) :: taufield,omega_c,myphi,regmat
    integer,allocatable,dimension(:,:) :: isfluid_LB
    real*8 :: cssq, omega, fx_LB,fy_LB,u_wall,v_wall,tau,visc_LB,visc_LB_R, &
     tau_R,tau_B,visc_LB_B
    real*8 :: mydensR,mydensB,dynamic_visc_LB,consistency,flowindex, &
     cssqR,cssqB
    real*8 :: zeroshear,maxvisc,yieldst,zeroy,myerror,convlimit, &
     minvisc,minshear,maxshear,analytVmax,surf_tens,beta_st,dens_ratio, &
     alpha_r,alpha_b,hwidth,analytVmax2,hhlengthref,analytVint,width
    integer :: nx,ny,ncomp,step,stamp,mychunk
	integer :: mxrank,my_in,my_fin,nchunkmin,idrank,error,threads
	real*8,parameter :: pi_greek=3.141592653589793238462643383279502884d0
	real*8, allocatable,dimension(:,:,:) :: cmat
	logical :: lfaketwo=.false.
	logical :: lmatlab=.false.
	integer :: hlength,hhlength
	real*8 :: hlengthref
	real*8, parameter :: myzero=1.d-16
	logical, dimension(2), save :: lpbc=(/.false.,.false./)
	logical, save :: ldrop=.false.
	
    contains
	function dimenumb(inum)
 
    !***********************************************************************
    !    
    !     LBsoft function for returning the number of digits
    !     of an integer number
    !     originally written in JETSPIN by M. Lauricella et al.
    !    
    !     licensed under the 3-Clause BSD License (BSD-3-Clause)
    !     author: M. Lauricella
    !     last modification July 2018
    !    
    !***********************************************************************

      implicit none

      integer,intent(in) :: inum
      integer :: dimenumb
      integer :: i
      real*8 :: tmp

      i=1
      tmp=real(inum,kind=8)
      do
      if(tmp< 10.d0 )exit
        i=i+1
        tmp=tmp/ 10.0d0
      enddo

      dimenumb=i

      return

     end function dimenumb

    function write_fmtnumb(inum)
 
    !***********************************************************************
    !    
    !     LBsoft function for returning the string of six characters
    !     with integer digits and leading zeros to the left
    !     originally written in JETSPIN by M. Lauricella et al.
    !    
    !     licensed under the 3-Clause BSD License (BSD-3-Clause)
    !     author: M. Lauricella
    !     last modification July 2018
    !    
    !***********************************************************************
 
    implicit none

    integer,intent(in) :: inum
    character(len=6) :: write_fmtnumb
    integer :: numdigit,irest
    !real*8 :: tmp
    character(len=22) :: cnumberlabel
    numdigit=dimenumb(inum)
    irest=6-numdigit
    if(irest>0)then
        write(cnumberlabel,"(a,i8,a,i8,a)")"(a",irest,",i",numdigit,")"
        write(write_fmtnumb,fmt=cnumberlabel)repeat('0',irest),inum
    else
        write(cnumberlabel,"(a,i8,a)")"(i",numdigit,")"
        write(write_fmtnumb,fmt=cnumberlabel)inum
    endif
 
    return
    end function write_fmtnumb   
    
   
    endmodule
    
    
    
