clear all
clc

%nx=256
%    ny=129
    ncomp=2
    surf_tens=0.02d0
    beta_st=0.9d0
    mydens=1.d0
    dens_ratio=1.d0 %ratio rho1/rho2  rho_r/rho_b
    stamp=100 %every time step you print the outputfile
    endstep=10000
    tau=1.d0 %limit of 0.5 corresponds to zero viscosity lesse 0.55 relaxation time
    omega=1.d0/tau; %relaxation frequence
    cssq=1.0d0/3.0d0; %dt^2/dx^2 square of sound speed in lattice
    visc_LB=cssq*(tau-0.5d0); %tau=visc_LB/ccsq+0.5
    dynamic_visc_LB=visc_LB*mydens;
%inserthere
%    consistency=1.0d0*dynamic_visc_LB
%    flowindex=0.75d0 

    zeroshear=1.d-8   %shear that you use like minimum reference
    yieldst=0.d-5     %yeld stress
    %fx_LB=1.0d-7   
    %fy_LB=0.0d0
    myerror=1.d-3
    convlimit=1.d-8
    threads=3
    minvisc=0.001d0   
    hlengthref=(ny)*0.5d0-1.0
h=hlengthref;
hlength=ny/2;
h=hlength;
umax=((0.5 .*(hlengthref.^2.0).*fx_LB./dynamic_visc_LB));

%u1 for the upper fluid I
u1=@(y)(umax.*(1.0-((y.^2.0)./(hlengthref.^2.0))));



%function fun with varibles c11,c12,c21,c22, with functions containing:
%u1(L/2)=0, u2(-L/2)=0, u1(0)=u2(0), du1(0)/dy=du2(0)/dy 
%fun=@(c)[u1(hlength,c(1),c(2),c(3),c(4));u2(-hlength,c(1),c(2),c(3),c(4));u1(0,c(1),c(2),c(3),c(4))-u2(0,c(1),c(2),c(3),c(4));du1(0,c(1),c(2),c(3),c(4))-du2(0,c(1),c(2),c(3),c(4))];
%options = optimset('Display','on','TolFun',10^-18,'TolX',10^-20,'MaxFunEvals',10^4,'MaxIter',10^4);
%options = optimset('Display','on','TolFun',10^-18,'TolX',10^-20,'MaxFunEvals',10^4,'MaxIter',10^4,'FunValCheck','on');
%c=fsolve(fun,[1 1 1 1],options); %finding root of function for constants c11,c12,c21,c22

%plot of solution with found constants
%dy=1;
counter=1;
counter2=1
for i=1:hlength
%    u02(counter)=u2(i,c(1),c(2),c(3),c(4));
    counter=counter+1;
    counter2=counter2+1;
    disp(i);
end
counter=1;
for i=hlength+1:ny
%    u01(counter)=u1(i,c(1),c(2),c(3),c(4));
    counter=counter+1;
    counter2=counter2+1;
    disp(i);
end
%figure
%plot(-hlength:0,u02)
%hold on
%plot(0:hlength,u01)
%title(['flowindex=',num2str(flowindex),' consistency=',num2str(consistency),' du1(0)/dy=du2(0)/dy'])

%x = [complex(1,0) complex(2,0) ];
y = zeros(3,counter2-1);

%counter2=1
%for i=-hlength:0
%    x(1,counter2)=i;
%    x(2,counter2)=u2(i,c(1),c(2),c(3),c(4));
%    counter2=counter2+1;
%end

%for i=0:hlength
%    x(1,counter2)=i;
%    x(2,counter2)=u1(i,c(1),c(2),c(3),c(4));
%    counter2=counter2+1;
%end

%fileID = fopen('sol1.txt','w');
%fprintf(fileID,'%22s\n','# du1(0)/dy=du2(0)/dy ');
%fprintf(fileID,'%16.8g %16.8g \n',x);
%fclose(fileID);


%function kfun with varibles c11,c12,c21,c22, with functions containing:
%u1(L/2)=0, u2(-L/2)=0, u1(0)=u2(0), tau1(0)=tau2(0)

%kfun=@(c)[u1(hlengthref,c(1),c(2));u1(-hlengthref,c(1),c(2))];
%c=fsolve(kfun,x,options); %finding root of function for constants c11,c12,c21,c22
%kc=fsolve(kfun,[1 1],options);

%disp(x);
%for i=1:2
%    fprintf('%i %16.8g %16.8g\n', i, real(c(i)), imag(c(i)));
%end
%for i=1:2
%    fprintf('%i %16.8g\n', i, real(kc(i)));
%end


%c=kc;
dy=1;
counter=1;
counter2=1
for i=1:hlength
    myi=i-hlength-0.5;
    if myi < -hlengthref
      y(1,counter2)=i;
      y(2,counter2)=myi;
      y(3,counter2)=0.0;
      counter=counter+1;
      counter2=counter2+1;
    else
      u02(counter)=u1(myi);
      y(1,counter2)=i;
      y(2,counter2)=myi;
      y(3,counter2)=u1(myi);
      counter=counter+1;
      counter2=counter2+1;
    end
end
counter=1;
for i=hlength+1:ny
    myi=i-hlength-0.5;
    if myi > hlengthref
      y(1,counter2)=i;
      y(2,counter2)=myi;
      y(3,counter2)=0.0;
      counter=counter+1;
      counter2=counter2+1;
    else
      u01(counter)=u1(myi);
      y(1,counter2)=i;
      y(2,counter2)=myi;
      y(3,counter2)=u1(myi);
      counter=counter+1;
      counter2=counter2+1;
    end
end

fileID = fopen('solmio.txt','w');
%fprintf(fileID,'%22s\n','# tau1(0)=tau2(0)     ');
fprintf(fileID,'%20.10g %20.10g %20.10g \n',y);
fclose(fileID);


%figure
%plot(-hlength:0,u02)
%hold on
%plot(0:hlength,u01)
%title('solution for tau1(0)=tau2(0)')

