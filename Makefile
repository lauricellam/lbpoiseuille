#
# makefile
#


BINROOT = $(CURDIR)
FC=undefined
CC=undefined
FFLAGS=undefined
LDFLAGS=undefined
TYPE=undefined
EX=main.x
EXE = $(BINROOT)/$(EX)
SHELL=/bin/sh

def:	all

all:
	@echo "Error - you should specify a target machine!"
	@echo "Possible choices are:              "
	@echo "                                   "
	@echo "gfortran                           "
	@echo "gfortran-openmp                    "
	@echo "intel                              "
	@echo "intel-openmp                       "
	@echo "                                   "
	@echo "Possible choices for debugging are:"
	@echo "                                   "
	@echo "gfortran-debugger                  "
	@echo "intel-debugger                     "
	@echo "                                   "
	@echo "Please examine Makefile for further details "

help: all

gfortran:
	$(MAKE) CC=gcc \
	FC=gfortran \
	CFLAGS="-O2 -cpp -I $(CURDIR) -c" \
	FFLAGS="-O2 -cpp  -I $(CURDIR) -c" \
	LDFLAGS="-O2 -cpp -I $(CURDIR) -o" \
	TYPE=main \
	EX=$(EX) BINROOT=$(BINROOT) main
	
gfortran-openmp:
	$(MAKE) CC=gcc \
	FC=gfortran \
	CFLAGS="-O2 -fopenmp -cpp -DOPENMP -I $(CURDIR) -c" \
	FFLAGS="-O2 -fopenmp -cpp  -DOPENMP -I $(CURDIR) -c" \
	LDFLAGS="-O2 -fopenmp -cpp -DOPENMP -I $(CURDIR) -o" \
	TYPE=main \
	EX=$(EX) BINROOT=$(BINROOT) main
	
intel:
	$(MAKE) CC=icc \
	FC=ifort \
	CFLAGS="-O2 -I $(CURDIR) -c" \
	FFLAGS="-O2 -cpp -DINTEL -I $(CURDIR) -c" \
	LDFLAGS="-O2 -cpp -DINTEL -I $(CURDIR) -o" \
	TYPE=main \
	EX=$(EX) BINROOT=$(BINROOT) main
	
intel-openmp:
	$(MAKE) CC=icc \
	FC=ifort \
	CFLAGS="-O2 -openmp -cpp -DOPENMP -I $(CURDIR) -c" \
	FFLAGS="-O2 -openmp -cpp -DOPENMP -DINTEL -I $(CURDIR) -c" \
	LDFLAGS="-O2 -openmp -cpp -DOPENMP -DINTEL -I $(CURDIR) -o" \
	TYPE=main \
	EX=$(EX) BINROOT=$(BINROOT) main
	
gfortran-debugger:
	$(MAKE) CC=gcc \
	FC=gfortran \
	CFLAGS="-O0 -g -cpp -I $(CURDIR) -c" \
	FFLAGS="-O0 -g -fcheck=all -fbacktrace \
	-ffpe-trap=invalid,zero,overflow,underflow,denormal \
	-cpp -I $(CURDIR) -c" \
	LDFLAGS="-O0 -g -fcheck=all -fbacktrace \
	-ffpe-trap=invalid,zero,overflow,underflow,denormal \
	-cpp -I $(CURDIR) -o" \
	TYPE=main \
	EX=$(EX) BINROOT=$(BINROOT) main
	
intel-debugger:
	$(MAKE) CC=icc \
	FC=ifort \
	CFLAGS="-O0 -g -I $(CURDIR) -c" \
	FFLAGS="-O0 -g -check all -traceback -check bounds -cpp -DINTEL -I $(CURDIR) -c" \
	LDFLAGS="-O0 -g -check all -traceback -check bounds -cpp -DINTEL -I $(CURDIR) -o" \
	TYPE=main \
	EX=$(EX) BINROOT=$(BINROOT) main

main:vars_lb.o subs_lb.o main.o
	$(FC) $(LDFLAGS) $(EX) vars_lb.o subs_lb.o main.o
	
vars_lb.o:vars_lb.f90
	$(FC) $(FFLAGS) vars_lb.f90
	
subs_lb.o:subs_lb.f90
	$(FC) $(FFLAGS) subs_lb.f90
	
main.o:main.f90
	$(FC) $(FFLAGS) main.f90
	
clean:
	rm -rf *.mod *.o main.x

