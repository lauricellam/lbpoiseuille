
    program LBpoiseuille
    !used for parallization!
#ifdef _OPENMP
    use omp_lib
#endif    
	
	use vars_lb
	use subs_lb

	
    implicit none
    integer, parameter :: maxlen=120
    real*8 :: ts1,ts2,dprof,length,yy,syy,myerr,tempR,tempB,vmaxerr,myi,ratiodynvisc, &
     Mt,A1,A2,B2,C1,C2
    real*8 :: vxprofmax,rescal_vxprof,rescal_analytV,myc1,cI,cII,myf,myfd,dtemp,butta(3)
    real*8, allocatable :: vxprof(:),phasefield(:)
    complex(kind=16) :: qconsistency,qtemp,qflowindex,qfx_LB,qyy,qc1,qcI,qcII, &
     qhlengthref,qf,qfd,qdynamic_visc_LB,qanalytV,conjgqanalytV
    complex(kind=16), parameter :: qone=cmplx(1.d0,0.d0,kind=16)
    integer,dimension(8) :: dt
    character(len=maxlen) :: mycommand,newcommand,newcommand2
    integer :: i,ii,jj,kk,iframe,endstep,itemp,ic
    logical :: lcomplex=.false.,lcomplex2=.false.
    logical :: lexist,lvtk
    !
    !
    ! Variables
	!**************************
    step=1
    
	!**************************
	!**************************
	!**************************
	!**************************
    !!!!!!!!!!!!!DATA!!!!!!!!!!!!
    nx=256
    ny=128
    ncomp=2
    surf_tens=0.02d0
    beta_st=0.9d0
    mydensR=1.d0
    dens_ratio=10.d0 !ratio rho1/rho2  rho_r/rho_b
    lpbc=[.true.,.false.]
    !!!!!!!!!!!!dependent data to compute analytical solution!!!!!!!!!!!
    if(ncomp.ne.1)then
      mydensB=mydensR
      if(dens_ratio==1.d0)then
        mydensR=mydensB
        alpha_b=4.0d0/9.0d0
        alpha_r=alpha_b
        cssqR=3.d0/5.d0*(1.d0-alpha_r)
        cssqB=3.d0/5.d0*(1.d0-alpha_b)
      else
        mydensR=mydensB*dens_ratio
        alpha_b=4.0d0/9.0d0
        alpha_r=(1.d0/dens_ratio)*(alpha_b-1.d0)+1.d0 !4.10 of the book
        cssqR=3.d0/5.d0*(1.d0-alpha_r)
        cssqB=3.d0/5.d0*(1.d0-alpha_b)
      endif
    else
      alpha_r=4.0d0/9.0d0
      cssqR=3.d0/5.d0*(1.d0-alpha_r)
    endif
    
    stamp=100 !every time step you print the outputfile
    endstep=30000
    tau=1.d0 !limit of 0.5 corresponds to zero viscosity lesse 0.55 relaxation time
    omega=1.d0/tau !relaxation frequence
    cssq=1.0d0/3.0d0 !dt^2/dx^2 square of sound speed in lattice
    visc_LB=cssq*(tau-0.5d0) !tau=visc_LB/ccsq+0.5
    tau_R=1.d0
    tau_B=1.d0
    visc_LB_R=cssq*(tau_R-0.5d0)
    visc_LB_B=cssq*(tau_B-0.5d0)
    if(ncomp.ne.1)then
      dynamic_visc_LB=visc_LB_B*mydensB
    else
      dynamic_visc_LB=visc_LB_R*mydensR
    endif
    consistency=1.d0*visc_LB_R*mydensR !1.d-2!visc_LB*mydens  !consistency 
    !it seems that if consistency<dynamic_visc_LB there is not a solution since
    !c1 takes negative values and probably under fractional exponent it takes NAN
    flowindex=1.d0 !0.75d0   !my n
    zeroshear=1.d-12   !shear that you use like minimum reference
    yieldst=0.d-8     !yeld stress
    fx_LB=1.d-7   !external body force
    fy_LB=0.0d0
    myerror=0.d-3
    convlimit=1.d-8
    threads=4
    minvisc=0.001d0
    hwidth=2.d0
    lfaketwo=.true.
    lvtk=.true.
    width=3.d0
    
    ratiodynvisc=(mydensR*visc_LB_R)/(mydensB*visc_LB_B)


    
    !!!!!!!!!!!!!END!!!!!!!!!!!!!
    if(flowindex==1.d0)then
      !consistency=visc_LB*mydens
      maxvisc=consistency+yieldst/zeroshear  !minimum viscosity
      maxshear=1000.d0
    else
      maxvisc=consistency*(zeroshear**(flowindex-1.d0))+yieldst/zeroshear  !minimum viscosity
      maxshear=(minvisc/consistency)**(1.d0/(flowindex-1.d0))
    endif
    
    call init_random_seed(514)
    
    
    
    length=dble(ny-1)
    !hlength=dble(ny-1)*0.5d0
    !hlengthref=dble(ny-1)*0.5d0-0.5d0
    hlength=ny/2
    hhlength=ny/4
    hlengthref=dble(ny)*0.5d0-1.0d0
    hhlengthref=dble(ny)*0.25d0-1.0d0
    
    allocate(analytV(ny))
    if(ncomp==1)then
      if(flowindex==1.d0)then
        analytVmax=((0.5d0*(hlengthref**2.d0)*fx_LB/dynamic_visc_LB))
        ic=0
        do i=1,hlength
          myi=dble(i-hlength)-0.5d0
          ic=ic+1
          if(myi < -hlengthref)then
            analytV(ic)=0.d0
          else
            analytV(ic)=analytVmax*(1.d0-((myi**2.d0)/(hlengthref**2.d0)))
          endif
        enddo
        
        do i=hlength+1,ny
          myi=dble(i-hlength)-0.5d0
          if(myi > hlengthref)then
            analytV(ic)=0.d0
          else
            analytV(ic)=analytVmax*(1.d0-((myi**2.d0)/(hlengthref**2.d0)))
          endif
        enddo
      else
        if(lfaketwo)then
          analytVmax2=((0.5d0*(hlengthref**2.d0)*fx_LB/dynamic_visc_LB))
          analytVint=analytVmax2*(1.d0-((hhlengthref**2.d0)/(hlengthref**2.d0)))
          analytVmax=((fx_LB/consistency)**(1.d0/flowindex))*(flowindex/(flowindex+1))* &
           (hhlengthref**((flowindex+1.d0)/flowindex))
          ic=0
          do i=1,hlength
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(dabs(myi)>hhlengthref)then
              if(myi < -hlengthref)then
                analytV(ic)=0.d0
              else
                analytV(ic)=analytVmax2*(1.d0-((myi**2.d0)/(hlengthref**2.d0)))
              endif
            else
              analytV(ic)=analytVmax*(1.d0-((dabs(myi)/hhlengthref)**((flowindex+1.d0)/flowindex)))+&
               analytVint
            endif
          enddo
          do i=hlength+1,ny
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(dabs(myi)>hhlengthref)then
              if(dabs(myi) > hlengthref)then
                analytV(ic)=0.d0
              else
                analytV(ic)=analytVmax2*(1.d0-((myi**2.d0)/(hlengthref**2.d0)))
              endif
            else
              analytV(ic)=analytVmax*(1.d0-((dabs(myi)/hhlengthref)**((flowindex+1.d0)/flowindex)))+&
               analytVint
            endif
          enddo
        else
          analytVmax=((fx_LB/consistency)**(1.d0/flowindex))*(flowindex/(flowindex+1))* &
           (hlengthref**((flowindex+1.d0)/flowindex))
          ic=0
          do i=1,hlength
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(myi < -hlengthref)then
              analytV(ic)=0.d0
            else
              analytV(ic)=analytVmax*(1.d0-((dabs(myi)/hlengthref)**((flowindex+1.d0)/flowindex)))
            endif
          enddo
          
          do i=hlength+1,ny
            myi=dble(i-hlength)-0.5d0
            ic=ic+1 
            if(myi > hlengthref)then
              analytV(ic)=0.d0
            else
              analytV(ic)=analytVmax*(1.d0-((dabs(myi)/hlengthref)**((flowindex+1.d0)/flowindex)))
            endif
          enddo
        endif
      endif
    
    else !for ncomp>1
      if(flowindex .ne. 1.d0)then
          analytVmax2=((0.5d0*(hlengthref**2.d0)*fx_LB/dynamic_visc_LB))
          analytVint=analytVmax2*(1.d0-((hhlengthref**2.d0)/(hlengthref**2.d0)))
          analytVmax=((fx_LB/consistency)**(1.d0/flowindex))*(flowindex/(flowindex+1))* &
           (hhlengthref**((flowindex+1.d0)/flowindex))
          ic=0
          do i=1,hlength
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(dabs(myi)>hhlengthref)then
              if(myi < -hlengthref)then
                analytV(ic)=0.d0
              else
                analytV(ic)=analytVmax2*(1.d0-((myi**2.d0)/(hlengthref**2.d0)))
              endif
            else
              analytV(ic)=analytVmax*(1.d0-((dabs(myi)/hhlengthref)**((flowindex+1.d0)/flowindex)))+&
               analytVint
            endif
          enddo
          do i=hlength+1,ny
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(dabs(myi)>hhlengthref)then
              if(dabs(myi) > hlengthref)then
                analytV(ic)=0.d0
              else
                analytV(ic)=analytVmax2*(1.d0-((myi**2.d0)/(hlengthref**2.d0)))
              endif
            else
              analytV(ic)=analytVmax*(1.d0-((dabs(myi)/hhlengthref)**((flowindex+1.d0)/flowindex)))+&
               analytVint
            endif
          enddo
      else
          Mt=ratiodynvisc
          
          
          
          analytVmax2=((0.5d0*(hlengthref**2.d0)*fx_LB/dynamic_visc_LB))
          analytVint=analytVmax2*(1.d0-((hhlengthref**2.d0)/(hlengthref**2.d0)))
          analytVmax=(fx_LB/consistency)*0.5d0* &
           (hhlengthref**2.d0)
          ic=0
          do i=1,hlength
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(dabs(myi)>hhlengthref)then
              if(dabs(myi) > hlengthref)then
                analytV(ic)=0.d0
              else
                analytV(ic)=analytVmax2*(1.d0-((myi**2.d0)/(hlengthref**2.d0)))
              endif
            else
              analytV(ic)=analytVmax*(1.d0-((dabs(myi)/hhlengthref)**2.d0))+&
               analytVint
            endif
          enddo
          do i=hlength+1,ny
            myi=dble(i-hlength)-0.5d0
            ic=ic+1
            if(dabs(myi)>hhlengthref)then
              if(dabs(myi) > hlengthref)then
                analytV(ic)=0.d0
              else
                analytV(ic)=analytVmax2*(1.d0-((myi**2.d0)/(hlengthref**2.d0)))
              endif
            else
              analytV(ic)=analytVmax*(1.d0-((dabs(myi)/hhlengthref)**2.d0))+&
               analytVint
            endif
          enddo
      endif
    endif
    
    analytVmax=0.d0
    do i=1,ny
      analytVmax=max(analytVmax,analytV(i))
    enddo
    


    
    
    write(6,*)'analytVmax',analytVmax
    open(unit=231,file='solutiontwo.dat',status='replace') 
    do jj=1,ny
      write(231,'(2g20.10)')dble(jj),analytV(jj) !/analytVmax
    end do
    close(231)
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    continue
    call alloc_LB
    continue
    call geometry
    
    continue
    call LB_sim_data
    
    do jj=2,ny-2
      do ii=1,nx
        uxy(2,ii,jj)=0.d0!uxy(2,ii,jj)+myerror*gauss_noseeded(ii,jj,23,52)
      enddo
    enddo
    !uxy=0.d0
    continue
    call init_LB
    continue
    !
    
    call write_isfluid_VTI(0)
!    open(unit=307,file='isf.out',status='replace')
!    do jj=1,ny
!	  do ii=1,nx
!		 write(307,*) isfluid_LB(ii,jj)
!	  enddo    
!	enddo
!    close(307)
    continue
    !*********************************************************************!
    !*************************LBpoiseuille main **************************!
    !*********************************************************************!   
    !ts1 = omp_get_wtime();
    call date_and_time(values=dt)
    print '(i4, 5(a, i2.2))', dt(1), '/', dt(2), '/', dt(3), ' ', &
                              dt(5), ':', dt(6), ':', dt(7)
    !open threads for parallization
#ifdef _OPENMP    
    call OMP_SET_NUM_THREADS(threads)
#endif 
          !used when compiled in openmg
    !$omp parallel

    !omp conditional compilation with omp master meaning main thread
#ifdef _OPENMP
   !$omp master
   threads=OMP_GET_NUM_THREADS()
   !call OMP_SET_NUM_THREADS(THREADS)
   write(6,*)  'running parallel code on',threads,'processors', '/',omp_get_num_threads(), &
    omp_get_thread_num()
   ts1 = omp_get_wtime() 
   iframe=0
   call write_rho_VTI(iframe)
   continue
   call write_vel_VTI(iframe)
   !$omp end master
#else
   write(*,*)  'running sequential code'
   call cpu_time(ts1)
   iframe=0
   call write_rho_VTI(iframe)
   continue
   call write_vel_VTI(iframe)
   !synchronize threads
#endif  
    !$omp barrier
100 continue
   
    !do step=1,500
        !LB
         call moments(step)
         !$omp barrier
         continue
         call wet_wall
         !$omp barrier
         continue
         !call compute_grads
         !call regularized
		 call collision_EDM_NW
		 !if(ncomp>1)call surface_tension_2c
		 !call surface_tension_cap_stress_2c
		 call correct_momentEDM
		 continue
		 !$omp barrier
         !call mixed_bcs
         call periodic_bcs
         !call fullway_bb
         continue
         !$omp barrier
         call streaming_opt
         continue
         if(mod(step,stamp)==0)then
           !$omp master
           iframe=iframe+1
           write(6,'(a,2i8,3g16.8,6f16.8)')'writing at step : ',step,iframe, &
            dabs(maxval(uxy)-analytVmax), &
            maxval(uxy),analytVmax,minval(taufield),maxval(taufield)
           if(lvtk)then
             call write_rho_VTI(iframe)
             continue
             call write_vel_VTI(iframe)
           endif
           !$omp end master
         endif
         !call fullway_bb
         continue
    !enddo
    !$omp master
        step = step +1 
    !$omp end master
    !$omp barrier
    if (step <=endstep) goto 100
    
    
    !*********************************************************************!
    !*********************************************************************!
    !*********************************************************************! 
#ifdef _OPENMP
    !$omp master
    write(*,*)  'runned in parallel mode on',threads,'processors', '/',omp_get_num_threads(), &
     omp_get_thread_num()
    ts2 = omp_get_wtime()
    !$omp end master
    !$omp barrier
#else
    write(*,*)  'runned in sequential mode'
    call cpu_time(ts2)
#endif  
    !$omp end parallel
    write(6,*)'execution time :', ts2-ts1
    
    call write_rho_VTI(iframe)
    call write_vel_VTI(iframe)
    
    open(unit=23,file='solution.dat',status='replace')
    vxprofmax=0.d0
    allocate(vxprof(ny),phasefield(ny))
    vxprof=0.d0
    phasefield=0.d0
    do jj=1,ny
      vxprof(jj)=0.d0
      yy=dble(i-hlength)-0.5d0
      tempR=0.d0
      tempB=0.d0
      do ii=1,nx
        vxprof(jj)=vxprof(jj)+uxy(1,ii,jj)
        tempR=tempR+rho(1,ii,jj)
        if(ncomp>1)tempB=tempB+rho(2,ii,jj)
      enddo
      vxprof(jj)=vxprof(jj)/dble(nx)
      tempR=tempR/dble(nx)
      tempB=tempB/dble(nx)
      if(ncomp>1)then
        if((tempR/mydensR+tempB/mydensB)==0.d0)then
          phasefield(jj)=0.d0
        else
          phasefield(jj)=(tempR/mydensR-tempB/mydensB)/(tempR/mydensR+tempB/mydensB)
        endif
      else
        phasefield(jj)=0.d0
      endif
      vxprofmax=max(vxprofmax,vxprof(jj))
    enddo
    
    do jj=1,ny
      myerr=dabs(vxprof(jj)-analytV(jj))
      if(analytV(jj)==0.d0)myerr=0.d0
      if(analytVmax/=0.d0)then
        rescal_analytV=analytV(jj)/analytVmax
      else
        rescal_analytV=0.d0
      endif
      if(vxprofmax/=0.d0)then
        rescal_vxprof=vxprof(jj)/vxprofmax
      else
        rescal_vxprof=0.d0
      endif
      write(23,*)dble(jj),yy,rescal_analytV,rescal_vxprof,myerr, &
       vxprof(jj),analytV(jj),phasefield(jj)
    enddo
    close(23)
    
    call date_and_time(values=dt)
    print '(i4, 5(a, i2.2))', dt(1), '/', dt(2), '/', dt(3), ' ', &
                              dt(5), ':', dt(6), ':', dt(7)
                              
   write(6,*)'lcomplex = ', lcomplex
                              
    end program LBpoiseuille
    
    

